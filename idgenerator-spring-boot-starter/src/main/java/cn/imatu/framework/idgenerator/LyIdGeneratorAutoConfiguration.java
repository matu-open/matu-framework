package cn.imatu.framework.idgenerator;

import cn.imatu.framework.idgenerator.config.IdGeneratorConfig;
import cn.imatu.framework.idgenerator.config.IdGeneratorProperties;
import com.github.yitter.contract.IdGeneratorOptions;
import com.github.yitter.idgen.YitIdHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@Slf4j
@AutoConfiguration
@EnableConfigurationProperties(IdGeneratorProperties.class)
@ImportAutoConfiguration(IdGeneratorConfig.class)
public class LyIdGeneratorAutoConfiguration {
    @Resource
    private IdGeneratorOptions idGeneratorOptions;

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());

        // 保存参数（必须的操作，否则以上设置都不能生效）：
        YitIdHelper.setIdGenerator(idGeneratorOptions);
        // 以上初始化过程只需全局一次
    }

}
