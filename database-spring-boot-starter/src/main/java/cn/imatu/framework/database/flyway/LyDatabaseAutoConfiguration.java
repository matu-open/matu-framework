package cn.imatu.framework.database.flyway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Slf4j
@Import({MyFlywayConfig.class})
@EnableConfigurationProperties(MyFlywayProperties.class)
@AutoConfigureBefore({DataSourceAutoConfiguration.class})
public class LyDatabaseAutoConfiguration {
    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
