package cn.imatu.framework.mybatis.mate;

import cn.imatu.framework.mybatis.mate.config.MybatisInterceptorCustomConfig;
import cn.imatu.framework.mybatis.mate.fieldunique.FieldUniqueCheckAspect;
import cn.imatu.framework.mybatis.mate.fieldunique.FieldUniqueCheckRedisLock;
import cn.imatu.framework.mybatis.mate.fieldunique.FieldUniqueCore;
import cn.imatu.framework.mybatis.mate.fieldunique.IFieldUniqueCheckLock;
import cn.imatu.framework.mybatis.mate.mapper.MybatisCommonMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 如果使用了自定义sql 且有字段使用了自动填充,则xml中这些字段不要做空判断
 * <a href="https://www.cnblogs.com/siroinfo/p/13095637.html">文章地址</a>
 *
 * @author shenguangyang
 */
@MapperScan(basePackages = "cn.imatu.framework.mybatis.mate.mapper")
@Import({
        FieldUniqueCheckAspect.class, MybatisInterceptorCustomConfig.class, FieldUniqueCheckRedisLock.class
})
public class LyMybatisMateAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyMybatisMateAutoConfiguration.class);
    @Resource
    private MybatisCommonMapper mybatisCommonMapper;

    @Autowired(required = false)
    private IFieldUniqueCheckLock fieldUniqueCheckLock;

    @PostConstruct
    public void init() {
        FieldUniqueCore.setFieldUniqueCheckLock(fieldUniqueCheckLock);
        FieldUniqueCore.setMybatisCommonMapper(mybatisCommonMapper);
        log.info("init {}", this.getClass().getName());
    }
}
