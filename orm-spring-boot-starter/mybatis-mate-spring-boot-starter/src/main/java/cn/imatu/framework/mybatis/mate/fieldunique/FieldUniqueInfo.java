package cn.imatu.framework.mybatis.mate.fieldunique;

import cn.imatu.framework.mybatis.mate.annotations.FieldUnique;
import cn.imatu.framework.mybatis.mate.annotations.FieldUniqueConfig;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 存放需要实体类中标注 {@link FieldUnique} 注解的信息
 *
 * @author shenguangyang
 */
@Getter
@Setter
public class FieldUniqueInfo {
    private FieldUniqueConfig fieldUniqueConfig;
    private Class<?> targetClass;
    private String tableName;
    /**
     * 表id名称
     */
    private String tableIdName;
    /**
     * 实体类中字段id名称
     */
    private String entityIdName;

    private Map<FieldUnique.Condition, List<FieldUniqueDefinition>> fieldUniqueDefinitionMap = new ConcurrentHashMap<>();


    public void add(FieldUniqueDefinition fieldUniqueDefinition) {
        fieldUniqueDefinitionMap
                .computeIfAbsent(fieldUniqueDefinition.getFieldUnique().condition(), (k) -> new ArrayList<>())
                .add(fieldUniqueDefinition);
    }

    @Getter
    @Setter
    public static class FieldUniqueDefinition {
        private FieldUnique fieldUnique;
        private Field field;
    }
}
