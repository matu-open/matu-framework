package cn.imatu.framework.mybatis.mate.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author shenguangyang
 */
@Mapper
public interface MybatisCommonMapper {
    @Select("${sqlStr}")
    List<Map<String, Object>> count(@Param(value = "sqlStr") String sqlStr);
}
