package cn.imatu.framework.mybatis.plus.config;


import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "mybatis-plus")
public class MybatisPlusExtendProperties {

    /**
     * 自定义id生成器配置
     */
    private CustomIdGenerator customIdGenerator;

    @Getter
    @Setter
    public static class CustomIdGenerator {
        /**
         * 是否使能采用漂移雪花算法生成数据库主键
         * 如果使用了自定义主键类型, 不需要指定type, @TableId(value = "demo_id")
         */
        private Boolean enableYitId;

        public CustomIdGenerator() {
            this.enableYitId = false;
        }
    }
}
