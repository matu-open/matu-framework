package cn.imatu.framework.mybatis.plus;

/**
 * @author shenguangyang
 * @date 2022-08-15 8:54
 */
public class GetDbNameFromUrlTest {
    public static void main(String[] args) {
        // String url = "jdbc:mysql://192.168.116.131:53306/ums1?useUnicode=true&characterEncoding=utf8&characterSetResults=utf8&serverTimezone=Asia/Shanghai";
        String url = "jdbc:mysql://192.168.116.131:53306/ums1";
        url = url.substring(url.indexOf("//") + 2);
        url = url.substring(url.indexOf("/") + 1);
        int i = url.indexOf("?");
        String dbName = "";
        dbName = i != -1 ? url.substring(0, i) : url;

        String inpValue = "11'111\\1";
        //意思是:  匹配不含这些特殊字符的其他任意一个或多个字符
        String regex = "^.*(=|&|/|\\\\).*$";
        if (dbName.matches(regex)) {
            throw new RuntimeException("url [ " + dbName + " ] is illegal");
        }
        System.out.println(dbName);

    }
}
