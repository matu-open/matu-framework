package cn.imatu.framework.mybatis.mate.enums;

/**
 * @author shenguangyang
 */
public enum SqlConditionTypeEnum {
    AND("and"),
    OR("or");

    private final String value;

    SqlConditionTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
