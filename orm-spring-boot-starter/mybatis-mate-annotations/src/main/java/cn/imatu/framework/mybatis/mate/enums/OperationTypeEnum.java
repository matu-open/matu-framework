package cn.imatu.framework.mybatis.mate.enums;

/**
 * 操作类型
 *
 * @author shenguangyang
 */
public enum OperationTypeEnum {
    UPDATE, SAVE
}
