package cn.imatu.framework.storage.base.config;

import lombok.Getter;
import lombok.Setter;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public abstract class BaseStorageProperties {
    /**
     * 存储服务器的地址
     */
    protected String endpoint = "http://127.0.0.1";

    protected String bucketName;
}
