package cn.imatu.framework.storage.base.config;

import cn.imatu.framework.core.constant.LyCoreConstants;
import cn.imatu.framework.storage.base.constants.FileStorageEnum;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: 存储配置
 *
 * @author shenguangyang
 */
@Getter
@Setter
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "storage")
public class StorageProperties {
    private static final Logger log = LoggerFactory.getLogger(StorageProperties.class);

    /**
     * 是否初始化桶
     */
    private Boolean initBucket = true;
    /**
     * 存储器
     */
    private FileStorageEnum storage;

    /**
     * 在项目启动的时候初始哈客户端
     */
    private Boolean initClient = true;
}