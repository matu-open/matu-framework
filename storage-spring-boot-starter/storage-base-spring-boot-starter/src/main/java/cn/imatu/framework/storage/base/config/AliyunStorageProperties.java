package cn.imatu.framework.storage.base.config;

import cn.imatu.framework.core.constant.LyCoreConstants;
import cn.imatu.framework.tool.core.UrlUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "storage.aliyun")
public class AliyunStorageProperties extends BaseStorageProperties {
    private String accessKeyId;
    private String secretAccessKey;

    @PostConstruct
    public void init() {
        this.setEndpoint(UrlUtils.addEndSlash(this.getEndpoint()));
    }
}
