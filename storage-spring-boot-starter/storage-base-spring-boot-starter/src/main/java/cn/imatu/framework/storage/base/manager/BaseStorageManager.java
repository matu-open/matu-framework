package cn.imatu.framework.storage.base.manager;

import cn.imatu.framework.core.utils.spring.SpringUtils;
import cn.imatu.framework.storage.base.config.StorageProperties;
import cn.imatu.framework.storage.base.constants.FileStorageEnum;
import cn.imatu.framework.tool.core.exception.Assert;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author shenguangyang
 */
public abstract class BaseStorageManager implements StorageManager {
    private static StorageProperties storageProperties;
    private static final Map<FileStorageEnum, StorageManager> storageManagerCache = new ConcurrentHashMap<>();

    @PostConstruct
    public void initBaseStorageManager() {
        storageProperties = SpringUtils.getBean(StorageProperties.class);
        storageManagerCache.put(this.storageType(), this);
    }

    public static StorageManager get() {
        StorageManager storageManager = storageManagerCache.get(storageProperties.getStorage());
        Assert.notNull(storageManager, "未实现 " + storageProperties.getStorage() + " 存储器, 请联系提供者");
        return storageManager;
    }

    public static StorageManager get(FileStorageEnum storage) {
        StorageManager storageManager = storageManagerCache.get(storage);
        Assert.notNull(storageManager, "未实现 " + storage + " 存储器, 请联系提供者");
        return storageManager;
    }
    
    public abstract FileStorageEnum storageType();
}
