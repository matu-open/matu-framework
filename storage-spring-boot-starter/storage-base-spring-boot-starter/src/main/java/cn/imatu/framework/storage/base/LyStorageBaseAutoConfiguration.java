package cn.imatu.framework.storage.base;

import cn.imatu.framework.storage.base.config.AliyunStorageProperties;
import cn.imatu.framework.storage.base.config.MinioStorageProperties;
import cn.imatu.framework.storage.base.config.StorageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@EnableConfigurationProperties({StorageProperties.class, MinioStorageProperties.class, AliyunStorageProperties.class})
public class LyStorageBaseAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyStorageBaseAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
