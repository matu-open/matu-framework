package cn.imatu.framework.storage.base.partupload;

import java.io.File;

/**
 * 分片上次服务
 *
 * @author shenguangyang
 */
public interface MultiPartUploadService {
    /**
     * 初始化
     *
     * @param objectName 对象名
     * @param totalPart  一共多少片
     */
    MultiPartUploadInit init(String objectName, int totalPart);

    /**
     * 上次分片
     *
     * @param uploadUrl   上传的url
     * @param contentType 类型
     * @param file        文件
     */
    void uploadPart(String uploadUrl, String contentType, File file);

    void merge(String objectName, String uploadId);
}
