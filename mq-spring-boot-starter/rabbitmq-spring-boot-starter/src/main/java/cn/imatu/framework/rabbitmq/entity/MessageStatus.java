package cn.imatu.framework.rabbitmq.entity;

/**
 * @author shenguangyang
 */
public enum MessageStatus {
    DELIVER_SUCCESS(1, "消息投递成功"),
    DELIVER_FAIL(-1, "消息投递失败"),
    CONSUMED_SUCCESS(2, "消息消费成功"),
    CONSUMED_FAIL(-2, "消息消费失败");

    private final int status;
    private final String message;

    MessageStatus(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
