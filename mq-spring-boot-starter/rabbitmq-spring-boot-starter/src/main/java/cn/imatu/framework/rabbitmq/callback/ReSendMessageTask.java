package cn.imatu.framework.rabbitmq.callback;

import com.alibaba.fastjson2.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.List;

/**
 * 定时从缓存中拉取发送失败消息重新投递到消息队列中
 *
 * @author shenguangyang
 */
public class ReSendMessageTask {
    private static final Logger log = LoggerFactory.getLogger(ReSendMessageTask.class);

    @Resource
    private MqSendFailService<RabbitSendFailMqMessage> sendFailService;

    @Autowired(required = false)
    private MqSendFailHandler mqSendFailHandler;

    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * 使用方自己通过分布式定时任务进行调用
     */
    public void reSend() {
        log.debug("task --- start resending failed messages");
        List<RabbitSendFailMqMessage> rabbitMqMessages = sendFailService.get();
        for (RabbitSendFailMqMessage rabbitMqMessage : rabbitMqMessages) {
            if (rabbitMqMessage.isMaxRetryCount()) {
                log.error("the message reaches the maximum number of reposts: {}", rabbitMqMessage.getMsgId());
                // 需要人工干预处理
                if (isHasMqSendFailHandle()) {
                    mqSendFailHandler.reachMaxRetryCount(rabbitMqMessage);
                }
                // 删除缓存记录
                sendFailService.delete(rabbitMqMessage.getMsgId());
            } else {
                log.debug("re-delivery this message : {}", rabbitMqMessage.getMsgId());
                CorrelationData correlationData = new CorrelationData();
                correlationData.setId(rabbitMqMessage.getMsgId());
                try {
                    rabbitTemplate.convertAndSend(rabbitMqMessage.getExchange(), rabbitMqMessage.getRoutingKey(), JSON.toJSONString(rabbitMqMessage.getMessage()), correlationData);
                } catch (AmqpException e) {
                    log.error("re-delivery this message fail: {}", rabbitMqMessage.getMsgId());
                }
                rabbitMqMessage.nextRetryCount();
                sendFailService.updateByMsgId(rabbitMqMessage);
            }
        }
        log.debug("task --- end resending failed messages");
    }

    public boolean isHasMqSendFailHandle() {
        return mqSendFailHandler != null;
    }
}
