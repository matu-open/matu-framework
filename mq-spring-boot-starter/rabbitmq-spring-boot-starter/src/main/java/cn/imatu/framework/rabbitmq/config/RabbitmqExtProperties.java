package cn.imatu.framework.rabbitmq.config;

import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 扩展属性
 *
 * @author shenguangyang
 */
@Getter
@Setter
@ConfigurationProperties(
        prefix = LyCoreConstants.PROPERTIES_PRE + "rabbitmq.publisher"
)
public class RabbitmqExtProperties {
    private Retry retry = new Retry();

    @Getter
    @Setter
    public static class Retry {
        /**
         * 最大重试次数
         */
        private int maxAttempts = 3;
    }
}
