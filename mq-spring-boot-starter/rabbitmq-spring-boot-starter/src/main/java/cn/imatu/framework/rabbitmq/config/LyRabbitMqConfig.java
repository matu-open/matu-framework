package cn.imatu.framework.rabbitmq.config;

import cn.imatu.framework.mq.base.config.MqProperties;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@Configuration
public class LyRabbitMqConfig {

    @Resource
    private MqProperties properties;

    @Bean("customContainerFactory")
    @ConditionalOnMissingBean(name = "customContainerFactory")
    public SimpleRabbitListenerContainerFactory containerFactory(SimpleRabbitListenerContainerFactoryConfigurer configurer,
                                                                 ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();

        // 并发数量:根据实际的服务器性能进行配置即可
        MqProperties.RabbitMq rabbitMq = properties.getRabbitMq();
        factory.setConcurrentConsumers(rabbitMq.getConcurrentConsumers());
        factory.setMaxConcurrentConsumers(rabbitMq.getMaxConcurrentConsumers());
        configurer.configure(factory, connectionFactory);
        return factory;
    }
}
