package cn.imatu.framework.rabbitmq.callback;

import cn.imatu.framework.cache.core.service.CacheService;
import cn.imatu.framework.mq.base.enums.MqCacheHashKey;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author shenguangyang
 */
public class MqSendFailServiceImpl implements MqSendFailService<RabbitSendFailMqMessage> {
    @Resource
    private CacheService cacheService;

    @Override
    public List<RabbitSendFailMqMessage> get() {
        List<RabbitSendFailMqMessage> result = new ArrayList<>();
        MqCacheHashKey mqCacheHashKey = MqCacheHashKey.FAIL_MESSAGE;
        Map<String, RabbitSendFailMqMessage> allMap = cacheService.opsForHash().getAll(mqCacheHashKey.getKey());
        for (Map.Entry<String, RabbitSendFailMqMessage> entry : allMap.entrySet()) {
            result.add(entry.getValue());
        }
        return result;
    }

    @Override
    public RabbitSendFailMqMessage get(String msgId) {
        MqCacheHashKey mqCacheHashKey = MqCacheHashKey.FAIL_MESSAGE;
        return cacheService.opsForHash().get(mqCacheHashKey.getKey(), mqCacheHashKey.formatHashKey(msgId));
    }

    @Override
    public void save(RabbitSendFailMqMessage message) {
        MqCacheHashKey key = MqCacheHashKey.FAIL_MESSAGE;
        cacheService.opsForHash().put(key.getKey(), key.formatHashKey(message.getMsgId()), message);
    }

    @Override
    public void updateByMsgId(RabbitSendFailMqMessage rabbitMqMessage) {
        save(rabbitMqMessage);
    }

    @Override
    public void delete(String msgId) {
        MqCacheHashKey key = MqCacheHashKey.FAIL_MESSAGE;
        cacheService.opsForHash().delete(key.getKey(), key.formatKey(msgId));
    }
}
