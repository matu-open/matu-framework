package cn.imatu.framework.rabbitmq.callback;

import cn.imatu.framework.mq.base.BaseMqMessage;

/**
 * mq 消息数据
 *
 * @author shenguangyang
 */
public class RabbitSendFailMqMessage extends BaseMqMessage {
    private static final int MAX_TRY_COUNT = 10;

    // 消息体
    private volatile Object message;
    // 交换机
    private String exchange;
    // 路由键
    private String routingKey;
    // 重试次数
    private int retryCount = 0;
    // 消息状态
    private int status;

    public void nextRetryCount() {
        this.retryCount = this.retryCount + 1;
    }

    /**
     * 是否达到最大重试次数
     *
     * @return true 已经达到最大重新投递次数
     */
    public boolean isMaxRetryCount() {
        return this.retryCount >= MAX_TRY_COUNT;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RabbitSendFailMqMessage{" +
                "message=" + message +
                ", exchange='" + exchange + '\'' +
                ", routingKey='" + routingKey + '\'' +
                ", retryCount=" + retryCount +
                ", status=" + status +
                '}';
    }
}
