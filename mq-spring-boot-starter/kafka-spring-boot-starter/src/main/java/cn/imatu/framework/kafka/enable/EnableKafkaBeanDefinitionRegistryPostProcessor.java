package cn.imatu.framework.kafka.enable;

import cn.imatu.framework.mq.base.domain.MqEnable;
import cn.imatu.framework.mq.base.enums.MqTypeEnum;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

/**
 * BeanDefinitionRegistryPostProcessor 后置处理器, 这里用于判断是否使能rocketmq
 * 如果不使能则移除, 相关自动配置类
 *
 * @author shenguangyang
 */
public class EnableKafkaBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    public EnableKafkaBeanDefinitionRegistryPostProcessor() {
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
        if (MqEnable.isEnabled(MqTypeEnum.KAFKA)) {
            return;
        }
        for (String beanDefinitionName : beanDefinitionRegistry.getBeanDefinitionNames()) {
            if (beanDefinitionName.toLowerCase().contains("kafka")) {
                beanDefinitionRegistry.removeBeanDefinition(beanDefinitionName);
            }
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

    }
}
