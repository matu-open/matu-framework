package cn.imatu.framework.kafka.enable;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 使能rocketmq
 *
 * @author shenguangyang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({EnableKafkaRegistrar.class})
public @interface EnableKafka {
    /**
     * 是否使能, 默认使能
     */
    boolean value() default true;
}
