package cn.imatu.framework.rocketmq.enable;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 使能rocketmq
 *
 * @author shenguangyang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({EnableRocketmqRegistrar.class})
public @interface EnableRocketmq {
    /**
     * 是否使能, 默认使能
     */
    boolean value() default true;
}
