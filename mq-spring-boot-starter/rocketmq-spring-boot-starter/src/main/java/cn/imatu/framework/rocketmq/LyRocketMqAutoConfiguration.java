package cn.imatu.framework.rocketmq;

import cn.imatu.framework.rocketmq.enable.EnableRocketmq;
import org.apache.rocketmq.client.log.ClientLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@EnableRocketmq(value = false)
@AutoConfiguration
public class LyRocketMqAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyRocketMqAutoConfiguration.class);

    @PostConstruct
    public void init() {
        System.setProperty(ClientLogger.CLIENT_LOG_USESLF4J, "true");
        log.info("init {}", this.getClass().getName());
    }
}
