package cn.imatu.framework.mqtt.service;

import cn.imatu.framework.mq.base.BaseMqMessage;
import cn.imatu.framework.mq.base.enums.QosEnum;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@Service
public class MqttSendExpandServiceImpl implements MqttSendExpandService {
    private static final Logger log = LoggerFactory.getLogger(MqttSendExpandServiceImpl.class);
    @Resource
    private MqttSendService mqttSendService;

    @Override
    public <T extends BaseMqMessage> void send(String topic, QosEnum qosEnum, T message) {
        if (message == null) {
            log.warn("message is null, abandon this delivery");
            return;
        }
        mqttSendService.sendToMqtt(topic, qosEnum.getValue(), new Gson().toJson(message));
    }
}
