package cn.imatu.framework.mqtt.listener;

import cn.imatu.framework.mq.base.enums.QosEnum;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;

import java.lang.annotation.*;

/**
 * 在监听者上标注该注解, 然后实现 {@link IMqttMessageListener} 接口
 * <p>
 * 示例代码如下
 * <blockquote><pre>
 * {@literal @Component}
 * {@literal @MqttMessageListener(topic = "test1", qos = QosEnum.QoS1)}
 * public class Demo1Listener implements IMqttListener, MqListener {
 *     private static final Logger log = LoggerFactory.getLogger(Demo1Listener.class);
 *     {@literal @Override}
 *     public void onMessage(String topic, String message) {
 *         log.info("topic: {}, message: {}", topic, message);
 *     }
 * }
 * </pre></blockquote>
 * </p>
 *
 * @author shenguangyang
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MqttMessageListener {
    String[] topic();

    QosEnum[] qos();
}
