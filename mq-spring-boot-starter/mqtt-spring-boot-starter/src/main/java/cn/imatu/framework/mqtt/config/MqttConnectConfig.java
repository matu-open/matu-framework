package cn.imatu.framework.mqtt.config;

import cn.imatu.framework.tool.core.StringUtils;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;

/**
 * @author shenguangyang
 */
public class MqttConnectConfig {

    /**
     * 客户端与服务器之间的连接意外中断，服务器将发布客户端的“遗嘱”消息
     */
    private final byte[] WILL_DATA;

    public MqttConnectConfig() {
        WILL_DATA = "offline".getBytes();
    }

    public MqttConnectOptions getMqttConnectOptions(MqttProperties mqttProperties) {
        //MQTT连接设置
        MqttConnectOptions option = new MqttConnectOptions();
        //设置是否清空session,false表示服务器会保留客户端的连接记录，true表示每次连接到服务器都以新的身份连接
        option.setCleanSession(false);
        // 设置连接的地址
        option.setServerURIs(StringUtils.split(mqttProperties.getServerUri(), ","));
        if (mqttProperties.getUsername() != null && !"".equals(mqttProperties.getUsername())) {
            //设置连接的用户名
            option.setUserName(mqttProperties.getUsername());
        }
        if (mqttProperties.getPassword() != null && !"".equals(mqttProperties.getPassword())) {
            //设置连接的密码
            option.setPassword(mqttProperties.getPassword().toCharArray());
        }

        option.setConnectionTimeout(mqttProperties.getConnectionTimeout());
        // 设置会话心跳时间 单位为秒 服务器会每隔1.5*20秒的时间向客户端发送心跳判断客户端是否在线
        // 但这个方法并没有重连的机制
        option.setKeepAliveInterval(mqttProperties.getKeepAliveSeconds());
        //setWill方法，如果项目中需要知道客户端是否掉线可以调用该方法。设置最终端口的通知消息 TODO 没有收到心跳视为离线
        option.setWill("willTopic", WILL_DATA, 2, false);
        return option;
    }
}
