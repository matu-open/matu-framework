package cn.imatu.framework.mqtt.config;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Component
public class MqttEnabled {

    public static boolean enabled = false;

    @PostConstruct
    public void init() {
        enabled = true;
    }
}
