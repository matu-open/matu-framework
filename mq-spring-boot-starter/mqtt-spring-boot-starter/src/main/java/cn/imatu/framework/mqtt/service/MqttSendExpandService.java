package cn.imatu.framework.mqtt.service;

import cn.imatu.framework.mq.base.BaseMqMessage;
import cn.imatu.framework.mq.base.enums.QosEnum;

/**
 * @author shenguangyang
 */
public interface MqttSendExpandService {
    <T extends BaseMqMessage> void send(String topic, QosEnum qosEnum, T message) throws Exception;
}
