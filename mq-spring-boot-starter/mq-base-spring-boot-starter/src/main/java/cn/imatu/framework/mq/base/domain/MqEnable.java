package cn.imatu.framework.mq.base.domain;

import cn.imatu.framework.mq.base.enums.MqTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author shenguangyang
 */
@Component
public class MqEnable {
    private static final Logger log = LoggerFactory.getLogger(MqEnable.class);
    private static final String EMPTY_STR = "";
    private static final Map<MqTypeEnum, String> ENABLE_MQ_TYPE = new ConcurrentHashMap<>();

    public static void addEnableMq(MqTypeEnum mqTypeEnum) {
        if (mqTypeEnum != null) {
            ENABLE_MQ_TYPE.put(mqTypeEnum, EMPTY_STR);
            log.info("mq.type: {}", mqTypeEnum.name());
        }
    }

    public static boolean isEnabled(MqTypeEnum mqTypeEnum) {
        return ENABLE_MQ_TYPE.containsKey(mqTypeEnum);
    }

}
