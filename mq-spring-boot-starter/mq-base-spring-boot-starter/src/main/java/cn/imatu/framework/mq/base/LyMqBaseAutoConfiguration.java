package cn.imatu.framework.mq.base;

import cn.imatu.framework.mq.base.config.MqProperties;
import cn.imatu.framework.mq.base.manager.MqManager;
import cn.imatu.framework.mq.base.register.MqPropertiesRegister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

;

/**
 * @author shenguangyang
 */
@Import({
        MqPropertiesRegister.class, MqManager.class
})
//@MqListenerScan
@EnableConfigurationProperties(MqProperties.class)
public class LyMqBaseAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyMqBaseAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
