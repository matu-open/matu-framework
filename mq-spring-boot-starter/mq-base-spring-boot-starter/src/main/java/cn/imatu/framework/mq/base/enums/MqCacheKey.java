package cn.imatu.framework.mq.base.enums;

import cn.imatu.framework.cache.core.key.ICacheKey;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.concurrent.TimeUnit;

/**
 * @author shenguangyang
 */
@Getter
@RequiredArgsConstructor
public enum MqCacheKey implements ICacheKey {
    /**
     * 重复消费
     * arg1: 消息id
     */
    REPEAT_CONSUME("mq:repeat:consume::%s", 60 * 60, TimeUnit.MINUTES);
    private final String key;
    private final int expire;
    private final TimeUnit unit;
}
