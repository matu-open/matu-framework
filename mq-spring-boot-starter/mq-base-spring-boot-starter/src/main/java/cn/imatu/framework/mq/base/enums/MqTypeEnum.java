package cn.imatu.framework.mq.base.enums;

import cn.imatu.framework.exception.BizException;

/**
 * @author shenguangyang
 */
public enum MqTypeEnum {
    ROCKETMQ("rocketmq"),
    MQTT("mqtt"),
    RABBITMQ("rabbitmq"),
    KAFKA("kafka");
    private final String type;

    MqTypeEnum(String type) {
        this.type = type;
    }

    public static MqTypeEnum getByType(String type) {
        MqTypeEnum[] values = MqTypeEnum.values();
        for (MqTypeEnum mqTypeEnum : values) {
            if (mqTypeEnum.getType().equals(type)) {
                return mqTypeEnum;
            }
        }
        return null;
    }

    /**
     * 类型是否被支持
     *
     * @param type
     */
    public static void isSupported(String type) {
        MqTypeEnum[] values = MqTypeEnum.values();
        for (MqTypeEnum mqTypeEnum : values) {
            if (mqTypeEnum.getType().equals(type)) {
                return;
            }
        }
        throw new BizException("mqType [ " + type + " ] not supported, support mqType [ " +
                ROCKETMQ.getType() + " , " + MQTT.getType() + " , " + KAFKA.getType() + " , " + RABBITMQ.getType() + " ]"
        );
    }


    public String getType() {
        return type;
    }
}
