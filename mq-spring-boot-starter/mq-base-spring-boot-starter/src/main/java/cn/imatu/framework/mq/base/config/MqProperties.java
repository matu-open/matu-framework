package cn.imatu.framework.mq.base.config;

import cn.imatu.framework.core.constant.LyCoreConstants;
import cn.imatu.framework.mq.base.enums.MqTypeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author shenguangyang
 */
@Data
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "mq")
public class MqProperties {
    /**
     * 指定使用的mq类型
     */
    private MqTypeEnum type;

    private RabbitMq rabbitMq = new RabbitMq();

    @Data
    public static class RabbitMq {
        /**
         * 并发消费数量
         */
        private Integer concurrentConsumers = Runtime.getRuntime().availableProcessors() * 2 - 2;
        /**
         * 核心消费的最大数量
         */
        private Integer maxConcurrentConsumers = Runtime.getRuntime().availableProcessors() * 2 - 2;
    }
}
