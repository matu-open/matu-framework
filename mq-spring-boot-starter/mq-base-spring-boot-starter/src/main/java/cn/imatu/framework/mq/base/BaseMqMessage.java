package cn.imatu.framework.mq.base;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

/**
 * 消息实体需要继承该类
 *
 * @author shenguangyang
 */
@Getter
@Setter
public class BaseMqMessage implements Serializable {
    /**
     * 消息id
     */
    protected String msgId;

    public BaseMqMessage() {
        this.msgId = UUID.randomUUID().toString().replaceAll("-", "");
    }
}
