package cn.imatu.framework.web;

import cn.imatu.framework.core.config.ValidatorProperties;
import cn.imatu.framework.web.handler.GlobalExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@EnableConfigurationProperties({ValidatorProperties.class})
@Import({GlobalExceptionHandler.class})
public class LyWebAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyWebAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
