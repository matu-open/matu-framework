package cn.imatu.framework.sms.manager;

import cn.imatu.framework.sms.manager.entity.SmsBaseReps;
import cn.imatu.framework.sms.manager.entity.SmsBaseReq;

/**
 * 短信通用业务处理层
 *
 * @author shenguangyang
 */
public interface ISmsManager<Req extends SmsBaseReq, Reps extends SmsBaseReps> {
    default Reps send(Req req) {
        throw new UnsupportedOperationException("not find sms vendor");
    }
}
