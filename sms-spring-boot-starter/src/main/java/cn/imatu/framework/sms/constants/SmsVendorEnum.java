package cn.imatu.framework.sms.constants;

/**
 * 短信供应商
 *
 * @author shenguangyang
 */
public enum SmsVendorEnum {
    ALIYUN("aliyun", "阿里云sms"),
    TENCENT("tencent", "腾讯sms"),
    HUAWEI("huawei", "华为sms"),
    BAIDU("baidu", "百度sms"),
    ;

    private final String name;
    private final String description;

    SmsVendorEnum(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
