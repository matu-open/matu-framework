package cn.imatu.framework.knife4j.micro.servlet;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * api docket 注解
 */
@Target(ElementType.TYPE)
@Inherited
@Import(Knife4jDocketRegistrar.class)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiGroupConfig {
    /**
     * 默认为当前服务名, spring.application.name
     */
    String groupName() default "";
}