package cn.imatu.framework.knife4j.micro.servlet;

import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static cn.imatu.framework.knife4j.micro.servlet.LyKnife4jApiInfoProperties.PREFIX;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@ConfigurationProperties(prefix = PREFIX)
public class LyKnife4jApiInfoProperties {
    public static final String PREFIX = LyCoreConstants.PROPERTIES_PRE + "knife4j.info";
    private String title = "default";
    private String description = "default";
    private Contact contact = new Contact();
    private String version = "v1.0.0";

    @Getter
    @Setter
    public static class Contact {
        private String name = "admin";
        private String url;
        private String email = "admin@dev.com";
    }
}
