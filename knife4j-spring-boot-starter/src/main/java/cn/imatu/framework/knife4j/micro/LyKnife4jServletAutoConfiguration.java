package cn.imatu.framework.knife4j.micro;

import cn.imatu.framework.core.utils.spring.SpringUtils;
import cn.imatu.framework.knife4j.micro.reactive.LyKnife4jProperties;
import cn.imatu.framework.knife4j.micro.servlet.Knife4jConfig;
import cn.imatu.framework.knife4j.micro.servlet.Knife4jWebConfig;
import cn.imatu.framework.knife4j.micro.servlet.LyKnife4jApiInfoProperties;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Slf4j
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@EnableConfigurationProperties({LyKnife4jApiInfoProperties.class, LyKnife4jProperties.class})
@ImportAutoConfiguration({Knife4jConfig.class})
public class LyKnife4jServletAutoConfiguration {
    @Value("${server.port:8080}")
    private Integer port;

    @PostConstruct
    public void init() {
        // 触发初始化bean
        SpringUtils.getBeansOfType(GroupedOpenApi.class);
        log.info("init {}", this.getClass().getName());
        log.info("获取swagger json数据接口:  http://127.0.0.1:{}/{}", port, "v3/api-docs");
    }

    @Bean
    public Knife4jWebConfig knife4jWebConfig() {
        return new Knife4jWebConfig();
    }
}
