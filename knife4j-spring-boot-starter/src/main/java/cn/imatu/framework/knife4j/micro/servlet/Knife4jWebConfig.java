package cn.imatu.framework.knife4j.micro.servlet;

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Description:
 *
 * @author shenguangyang
 */
public class Knife4jWebConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        /*
         * springboot > 2.6
         * 注意，这一步是最核心的，由于springboot使用的是PathPatternMatcher，
         * 所以无法识别swagger的静态资源路径。网上很多的方法指导是将springboot2.6.x
         * 版本的路径匹配规则修改回AntPathMatcher，因此要改一堆配置。
         * 而直接添加addResourceHandlers的方式，能够避免复杂的配置工作。
         */
        registry.addResourceHandler("/swagger-ui/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/")
                .resourceChain(false);
    }
}
