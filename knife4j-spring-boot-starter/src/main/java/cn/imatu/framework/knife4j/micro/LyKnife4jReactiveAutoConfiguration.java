package cn.imatu.framework.knife4j.micro;

import cn.imatu.framework.knife4j.micro.reactive.Knife4jReactiveConfig;
import cn.imatu.framework.knife4j.micro.reactive.LyKnife4jProperties;
import cn.imatu.framework.knife4j.micro.reactive.SwaggerHeaderFilter;
import cn.imatu.framework.knife4j.micro.servlet.LyKnife4jApiInfoProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.SpringDocConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@Import({
        Knife4jReactiveConfig.class, SwaggerHeaderFilter.class,
        SpringDocConfiguration.class
})
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
@EnableConfigurationProperties({LyKnife4jProperties.class, LyKnife4jApiInfoProperties.class})
public class LyKnife4jReactiveAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyKnife4jReactiveAutoConfiguration.class);
    @Value("${server.port:8080}")
    private Integer port;

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
        log.info("gateway api doc url [http://127.0.0.1:{}/{}]", port, "doc.html");
    }
}
