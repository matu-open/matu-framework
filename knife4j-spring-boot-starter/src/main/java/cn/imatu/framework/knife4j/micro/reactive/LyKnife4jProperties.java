package cn.imatu.framework.knife4j.micro.reactive;

import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "knife4j")
public class LyKnife4jProperties {
    private Boolean enable = false;
    /**
     * 排除的uri , 支持正则匹配
     */
    private List<String> excludeUri = new ArrayList<>();
}
