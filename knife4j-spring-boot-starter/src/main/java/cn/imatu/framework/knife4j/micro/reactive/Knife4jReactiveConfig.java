package cn.imatu.framework.knife4j.micro.reactive;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@Configuration
@EnableKnife4j
public class Knife4jReactiveConfig {

    @Resource
    private LyKnife4jProperties knife4JExtProperties;

}
