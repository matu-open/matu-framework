package cn.imatu.framework.flyway.entity;

import cn.imatu.framework.flyway.FlywayCons;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author shenguangyang
 */
@Data
@TableName(value = FlywayCons.TABLE_NAME)
public class Flyway {
    @TableId
    private Integer installedRank;

    private Integer checksum;

    private String description;

    private Long executionTime;

    private String installedBy;

    private String installedOn;

    private String script;

    private Integer success;

    private String type;

    private String version;
}
