package cn.imatu.framework.flyway.properties;

import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author shenguangyang
 */
@Data
@Component
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "flyway")
public class LyFlywayProperties {
    /**
     * 是否使能
     */
    private Boolean enabled = true;

    /**
     * 表名
     */
    private String table = "flyway_schema_history";

    /**
     * Locations of migrations scripts
     */
    private List<String> locations = new ArrayList<>(Collections.singletonList("classpath*:db/migration/**/*.sql"));

}
