package cn.imatu.framework.flyway.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shenguangyang
 */
@Data
public class ResourceScript {
    private String type;
    private List<FlywaySql> sqlList = new ArrayList<>();
}
