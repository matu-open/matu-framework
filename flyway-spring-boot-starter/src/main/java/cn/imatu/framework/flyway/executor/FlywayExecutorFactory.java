package cn.imatu.framework.flyway.executor;

import cn.imatu.framework.flyway.FlywayException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@Component
public class FlywayExecutorFactory {
    @Resource(name = "mysqlFlywayExecutor")
    private MysqlFlywayExecutor mysqlFlywayExecutor;

    public FlywayExecutor getInstance(String databaseProductName) {
        FlywayExecutor instance;
        switch (databaseProductName) {
            case "MySQL":
                instance = mysqlFlywayExecutor;
                break;
            default:
                throw new FlywayException("尚未实现 {} 数据库执行器", databaseProductName);
        }
        return instance;
    }
}
