package cn.imatu.framework.flyway.executor;

import cn.imatu.framework.flyway.FlywayCons;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author shenguangyang
 */
@Slf4j
@Component
public class MysqlFlywayExecutor extends FlywayExecutor {

    @Override
    public void initTable() {
        String sql = String.format(
                "Create Table If Not Exists `%s` " +
                "( " +
                "`installed_rank` int NOT NULL, " +
                "`version` varchar(50)   DEFAULT NULL, " +
                "`description` varchar(200)   NOT NULL, " +
                "`type` varchar(32)   NOT NULL, " +
                "`script` varchar(2000)   NOT NULL, `checksum` int DEFAULT NULL, " +
                "`installed_by` varchar(100)   NOT NULL, " +
                "`installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                " `execution_time` int NOT NULL, `success` tinyint(1) NOT NULL, " +
                " PRIMARY KEY (`installed_rank`,`type`), " +
                " KEY `flyway_schema_history_s_idx` (`success`) USING BTREE " +
                ") ENGINE=InnoDB;",
                FlywayCons.TABLE_NAME
        );
        jdbcTemplate.execute(sql);
    }
}
