package cn.imatu.framework.flyway.mapper;

import cn.imatu.framework.flyway.entity.Flyway;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author shenguangyang
 */
@Mapper
public interface FlywayMapper extends BaseMapper<Flyway> {

}
