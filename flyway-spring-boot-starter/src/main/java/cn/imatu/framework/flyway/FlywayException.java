package cn.imatu.framework.flyway;

import cn.hutool.core.util.StrUtil;

/**
 * @author shenguangyang
 */
public class FlywayException extends RuntimeException {
    public FlywayException() {
        super();
    }

    public FlywayException(String message) {
        super(message);
    }

    public FlywayException(String message, Throwable cause) {
        super(message, cause);
    }

    public FlywayException(Throwable cause) {
        super(cause);
    }

    protected FlywayException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public FlywayException(String message, Object... params) {
        super(StrUtil.format(message, params));
    }
}
