package cn.imatu.framework.lock.exception;

import cn.imatu.framework.exception.BaseException;

/**
 * @author shenguangyang47
 */
public class UnLockException extends BaseException {
    public UnLockException(String message) {
        super(message);
    }
}
