package cn.imatu.framework.lock.model;

import java.util.concurrent.TimeUnit;

/**
 * 分布式锁通用接口
 *
 * @author shenguangyang
 */
public interface ILock {
    /**
     * 是否被上锁
     */
    boolean isLocked();

    /**
     * 带超时上锁
     */
    boolean tryLock(long waitTime, TimeUnit unit);

    /**
     * 一直等待直到获取到锁
     */
    void lock() ;

    /**
     * 解锁
     */
    void unlock();
}
