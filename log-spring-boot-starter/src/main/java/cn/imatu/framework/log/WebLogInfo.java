package cn.imatu.framework.log;

import cn.imatu.framework.log.annotation.LogEvent;
import cn.imatu.framework.log.annotation.OperateLog;
import cn.imatu.framework.log.common.BizStatus;
import com.alibaba.fastjson2.annotation.JSONField;
import com.alibaba.fastjson2.annotation.JSONType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作日志数据
 * {@code @JsonInclude(JsonInclude.Include.NON_EMPTY) }属性为 空（""） 或者为 NULL 都不序列化
 *
 * @author shenguangyang
 */
@Getter
@Setter
@Builder
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JSONType(orders = {"status", "costTime", "ip", "url", "httpMethod", "reqData", "respData"})
public class WebLogInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 自定义扩展参数
     */
    private Map<String, Object> extParams = new HashMap<>();

    /**
     * 事件对象
     */
    private String eventObject;

    /**
     * 事件对象名称
     */
    private String eventObjectName;

    /**
     * 上下文类型
     */
    private String contentType;

    /**
     * 请求方式, get / post等方式
     */
    private String httpMethod;

    /**
     * 请求url
     */
    private String url;

    /**
     * 操作地址
     */
    private String ip;

    /**
     * 请求参数
     */
    private Object reqData;

    /**
     * 返回参数
     */
    private Object respData;

    /**
     * 操作状态 ok/fail
     */
    private BizStatus status;

    /**
     * 错误消息
     */
    private String errorMsg;

    /**
     * 耗时单位是ms
     */
    private Long costTime;

    /**
     * 事件类型
     */
    private String eventType;

    /**
     * 事件类型名称
     */
    private String eventTypeName;

    /**
     * 日志内容
     */
    private String content;

    /**
     * 操作人类别
     */
    private String oprType;

    /**
     * 日志跟踪id
     */
    private String traceId;

    @JsonIgnore
    @JSONField(serialize = false, deserialize = false)
    private ProceedingJoinPoint proceedingJoinPoint;

    /**
     * 异常信息
     */
    @JsonIgnore
    @JSONField(serialize = false, deserialize = false)
    private Throwable throwable;

    /**
     * 请求方法
     */
    @JsonIgnore
    @JSONField(serialize = false, deserialize = false)
    private String classMethod;

    /**
     * 类上的注解
     */
    @JsonIgnore
    @JSONField(serialize = false, deserialize = false)
    private LogEvent logEventAnnot;

    /**
     * 方法上的注解
     */
    @JsonIgnore
    @JSONField(serialize = false, deserialize = false)
    private OperateLog operateLogAnnot;
}
