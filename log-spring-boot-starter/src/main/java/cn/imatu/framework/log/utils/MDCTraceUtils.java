package cn.imatu.framework.log.utils;

import cn.imatu.framework.log.common.LogCons;
import org.slf4j.MDC;

import java.util.UUID;

/**
 * Description:
 *
 * @author shenguangyang
 */
public class MDCTraceUtils {


    /**
     * filter的优先级，值越低越优先
     */
    public static final int FILTER_ORDER = -1;

    /**
     * 创建traceId并赋值MDC
     */
    public static void addTraceId() {
        MDC.put(LogCons.TRACE_ID, createTraceId());
    }

    /**
     * 赋值MDC
     */
    public static void putTraceId(String traceId) {
        MDC.put(LogCons.TRACE_ID, traceId);
    }

    /**
     * 获取MDC中的traceId值
     */
    public static String getTraceId() {
        return MDC.get(LogCons.TRACE_ID);
    }

    /**
     * 清除MDC的值
     */
    public static void removeTraceId() {
        MDC.remove(LogCons.TRACE_ID);
    }

    /**
     * 创建traceId
     */
    public static String createTraceId() {
        return UUID.randomUUID().toString().replace("-", "").toLowerCase();
    }
}
