package cn.imatu.framework.log.core.filter;

/**
 * 自定义跟踪用户相关定义
 *
 * @author shenguangyang
 */
@FunctionalInterface
public interface CustomTraceUser {
    String getLoginId();
}
