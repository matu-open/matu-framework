package cn.imatu.framework.log.common;

import cn.imatu.framework.log.WebLogInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.function.Consumer;

/**
 * 羚羊日志配置
 * @author shenguangyang
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AccessLogConfig {
    /**
     * 打印日志函数, 用于输出到控制台或者日志文件中
     */
    private Consumer<WebLogInfo> reqLogHandle;
    private Consumer<WebLogInfo> respLogHandle;

    /**
     * 默认事件类型
     */
    private String defaultEventType;

    /**
     * 默认操作类型
     */
    private String defaultOpsType;

    /**
     * 默认操作员类型
     */
    private String defaultOprType;
}
