package cn.imatu.framework.log.common;

/**
 * @author shenguangyang
 */
public enum BizStatus {
    /**
     * 成功
     */
    OK,

    /**
     * 失败
     */
    FAIL,
}
