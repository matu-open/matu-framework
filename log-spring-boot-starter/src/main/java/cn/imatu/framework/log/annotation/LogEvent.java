package cn.imatu.framework.log.annotation;

import java.lang.annotation.*;

/**
 * 自定义日志事件
 *
 * @author shenguangyang
 */
@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogEvent {
    /**
     * 事件对象, eg: systemUser 表示系统用户
     *
     * 该事件对象是标注在类上的, 而 {@link OperateLog#eventType()} 是标注在方法上的,
     * 所以当前事件对象可以包含很多个 {@link OperateLog#eventType()} 事件类型
     */
    String eventObject() default "";

    /**
     * 事件名称
     */
    String name() default "";
}
