package cn.imatu.framework.log;

import cn.imatu.framework.core.constant.LyCoreConstants;
import cn.imatu.framework.log.utils.LogUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Collections;
import java.util.List;

/**
 * @author shenguangyang
 */
@Data
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "log")
public class LyLogProperties {
    private Boolean enableWebLog = Boolean.TRUE;

    /**
     * 当发生错误输出堆栈时候, 只保留指定包下的堆栈信息, 一般为包名
     * @apiNote 只有在调用 {@link LogUtils#formatStackTrace(Throwable)} 才有效果
     */
    private List<String> filterStackTracePackages = Collections.emptyList();
}
