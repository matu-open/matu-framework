package cn.imatu.framework.log.common;

/**
 * @author shenguangyang
 */
public interface LogCons {
    String TRACE_ID = "traceId";
    /**
     * 登录id, 用户标识一个用户, 可以是用户id或者登录时候随机生成一个id, 默认为0
     */
    String LOGIN_ID = "X-LoginId";
    /**
     * 日志链路追踪id信息头, sleuth 框架使用的头部
     * 对于sleuth框架, 只要request请求中包含X-B3-SpanId，X-B3-ParentSpanId, X-B3-TraceId 则traceId才会生效
     *
     */
    String TRACE_ID_HEADER = "X-B3-TraceId";
    String SPAN_ID_HEADER = "X-B3-SpanId";
    String PARENT_SPAN_ID_HEADER = "X-B3-ParentSpanId";

    String PRE_APP_KEY = "x-pre-app-key";
    String PRE_APP_HOST_NAME = "x-pre-app-host-name";
    String PRE_APP_IP = "x-pre-app-ip";
    String WEBFLUX_EXCHANGE = "webflux-exchange";
}
