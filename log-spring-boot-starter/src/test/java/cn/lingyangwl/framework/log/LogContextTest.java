package cn.imatu.framework.log;

import cn.hutool.core.util.ClassUtil;

/**
 * @author shenguangyang
 */
class LogContextTest {
    public static void main(String[] args) {
        String t1 = "12";
        Long t2 = 1L;
        System.out.println(ClassUtil.isBasicType(t1.getClass()));
        System.out.println(ClassUtil.isBasicType(t2.getClass()));
        LogContext.initParams();
        LogContext.addParams("t1", "test2", "、");
        LogContext.addParams("t1", "test3", "、");
        LogContext.addParams("t1", "test4", "、");
        System.out.println(LogContext.params().get("t1"));
    }
}