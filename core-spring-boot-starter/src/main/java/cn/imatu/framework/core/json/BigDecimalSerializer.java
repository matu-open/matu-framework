package cn.imatu.framework.core.json;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * 主要用于 解决 BigDecimal 类型属性整数返回到前端不显示.00的问题
 *
 * 使用方式: 在响应属性标记注解 @JsonSerialize(using = BigDecimalSerializer.class)
 *
 * @author shenguangyang
 */
public class BigDecimalSerializer extends JsonSerializer<BigDecimal> {
 
    @Override
    public void serialize(BigDecimal value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (ObjUtil.isNotNull(value)) {
            // 保留2位小数，四舍五入
            jsonGenerator.writeString(NumberUtil.decimalFormat("##0.00", value));
        } else {
            jsonGenerator.writeString(NumberUtil.decimalFormat("##0.00", 0.00));
        }
    }
    
}