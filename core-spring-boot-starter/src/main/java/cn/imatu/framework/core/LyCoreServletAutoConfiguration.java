package cn.imatu.framework.core;


import cn.imatu.framework.core.response.ServletRequestHandlerImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Slf4j
@AutoConfiguration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Import({
        ServletRequestHandlerImpl.class,
})
public class LyCoreServletAutoConfiguration {
    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
