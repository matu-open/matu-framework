package cn.imatu.framework.core;

import cn.imatu.framework.core.config.LyThreadPoolConfig;
import cn.imatu.framework.core.config.properties.LyHttpClientProperties;
import cn.imatu.framework.core.config.properties.LyThreadPoolProperties;
import cn.imatu.framework.core.response.DefaultResponseInfo;
import cn.imatu.framework.core.response.EmptyRequestHandlerImpl;
import cn.imatu.framework.core.response.Resp;
import cn.imatu.framework.core.response.ResponseConfig;
import cn.imatu.framework.core.response.wrap.WebInitializingAdvice;
import cn.imatu.framework.core.utils.AsyncTaskExecutorWithTx;
import cn.imatu.framework.core.utils.spring.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnNotWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.context.request.RequestContextListener;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Import({
        IniEnvRegistrar.class,
        SpringUtils.class,
        LyThreadPoolConfig.class,
        AsyncTaskExecutorWithTx.class, ResponseConfig.class,
        // 响应相关的
        WebInitializingAdvice.class
})
@AutoConfiguration
@EnableConfigurationProperties({
        LyThreadPoolProperties.class, LyHttpClientProperties.class
})
public class LyCoreAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyCoreAutoConfiguration.class);

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
        Resp.setDefaultResponseInfo(SpringUtils.getBean(DefaultResponseInfo.class));
    }

    @Bean
    @ConditionalOnNotWebApplication
    public EmptyRequestHandlerImpl emptyRequestHandler() {
        return new EmptyRequestHandlerImpl();
    }
}