package cn.imatu.framework.core.response;

import lombok.*;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageData<T> implements Serializable {
    private Collection<T> records;
    private Long total = 0L;
    private Integer pageSize = 10;
    private Integer pageNum = 1;
}
