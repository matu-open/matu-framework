package cn.imatu.framework.core.constant;

/**
 * 请求常量
 *
 * @author shenguangyang
 */
public class LyCoreConstants {
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    public static final Integer SUCCESS = 200;

    /**
     * 配置文件前缀
     */
    public static final String PROPERTIES_PRE = "lingyang-framework.";
    /**
     * 没有点的前缀
     */
    public static final String PROPERTIES_PRE_NO_POINT = "lingyang-framework";

}
