package cn.imatu.framework.core.response.wrap;

import org.springframework.core.MethodParameter;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

/**
 * 包装返回结果
 *
 * @author shenguangyang
 */
public class WebWrapReturnValueHandler implements HandlerMethodReturnValueHandler {
    private final RequestResponseBodyMethodProcessor target;

    public WebWrapReturnValueHandler(RequestResponseBodyMethodProcessor target) {
        this.target = target;
    }

    @Override
    public boolean supportsReturnType(MethodParameter returnType) {
        return true;
    }

    @Override
    public void handleReturnValue(Object returnValue, MethodParameter methodParameter,
                                  ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest) throws Exception {
        target.handleReturnValue(returnValue, methodParameter, modelAndViewContainer, nativeWebRequest);
    }
}
