package cn.imatu.framework.core.http;

import cn.imatu.framework.core.constant.WebType;
import cn.imatu.framework.core.utils.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.server.ServerWebExchange;

import java.net.InetSocketAddress;
import java.util.Objects;

/**
 * @author shenguangyang
 */
@Slf4j
public class HttpRequestReactiveManager implements HttpRequestManager {
    private final ServerWebExchange exchange;

    public HttpRequestReactiveManager(ServerWebExchange exchange) {
        this.exchange = exchange;
    }

    @Override
    public String getHeader(String key) {
        return this.exchange.getRequest().getHeaders().getFirst(key);
    }

    @Override
    public String getRequestUri() {
        return this.exchange.getRequest().getPath().toString();
    }

    @Override
    public WebType getWebType() {
        return WebType.REACTIVE;
    }

    @Override
    public String getRequestIp() {
        return IpUtils.getRequestIp(this);
    }

    @Override
    public String getRemoteAddress() {
        InetSocketAddress remoteAddress = this.exchange.getRequest().getRemoteAddress();
        if (Objects.isNull(remoteAddress)) {
            return "";
        }
        return remoteAddress.getAddress().getHostAddress();
    }
}
