package cn.imatu.framework.core.response;

import lombok.*;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DefaultResponseInfo {
    /**
     * 成功状态码
     */
    private Integer success = 200;
    private String successMsg = "success";

    /**
     * 失败状态码
     */
    private Integer fail = 500;
    private String failMsg = "fail";
}
