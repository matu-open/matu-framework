
package cn.imatu.framework.core.utils.servlet;

import cn.hutool.core.convert.Convert;
import cn.imatu.framework.core.constant.*;
import cn.imatu.framework.core.response.*;
import cn.imatu.framework.core.utils.*;
import cn.imatu.framework.tool.core.*;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Miscellaneous utilities for web applications.
 *
 * @author shenguangyang
 */
@Slf4j
@UtilityClass
public class ServletUtils extends org.springframework.web.util.WebUtils {

    /**
     * 获取String参数
     */
    public static String getParameter(String name) {
        return getRequest().orElseThrow(() -> new RuntimeException("request is null")).getParameter(name);
    }

    /**
     * 获取String参数
     */
    public static String getParameter(String name, String defaultValue) {
        return Convert.toStr(getParameter(name), defaultValue);
    }

    /**
     * 获取Integer参数
     */
    public static Integer getParameterToInt(String name) {
        return Convert.toInt(getParameter(name));
    }

    /**
     * 获取Integer参数
     */
    public static Integer getParameterToInt(String name, Integer defaultValue) {
        return Convert.toInt(getParameter(name), defaultValue);
    }

    /**
     * 获取session
     */
    public static HttpSession getSession() {
        return getRequest().orElseThrow(() -> new RuntimeException("request is null")).getSession();
    }

    public static Optional<ServletRequestAttributes> getRequestAttributes() {
        try {
            RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
            return Optional.ofNullable((ServletRequestAttributes) attributes);
        } catch (Exception e) {
            log.error("error: {}", e.getMessage());
            return Optional.empty();
        }
    }

    public static String getHeader(HttpServletRequest request, String name) {
        String value = request.getHeader(name);
        if (StringUtils.isEmpty(value)) {
            return StringUtils.EMPTY;
        }
        return urlDecode(value);
    }

    public static Map<String, String> getHeaders() {
        Map<String, String> map = new LinkedHashMap<>();
        getRequest().ifPresent((request) -> {
            Enumeration<String> enumeration = request.getHeaderNames();
            if (enumeration != null) {
                while (enumeration.hasMoreElements()) {
                    String key = enumeration.nextElement();
                    String value = request.getHeader(key);
                    map.put(key, value);
                }
            }
        });
        return map;
    }

    /**
     * 将字符串渲染到客户端
     *
     * @param response 渲染对象
     * @param string   待渲染的字符串
     * @return null
     */
    public static String renderString(HttpServletResponse response, String string) {
        try {
            response.setStatus(200);
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            response.getWriter().print(string);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 是否是Ajax异步请求
     */
    public static boolean isAjaxRequest(HttpServletRequest request) {
        String accept = request.getHeader("accept");
        if (accept != null && accept.contains("application/json")) {
            return true;
        }

        String xRequestedWith = request.getHeader("X-Requested-With");
        if (xRequestedWith != null && xRequestedWith.contains("XMLHttpRequest")) {
            return true;
        }

        String uri = request.getRequestURI();
        if (StringUtils.inStringIgnoreCase(uri, ".json", ".xml")) {
            return true;
        }

        String ajax = request.getParameter("__ajax");
        return StringUtils.inStringIgnoreCase(ajax, "json", "xml");
    }

    /**
     * 内容编码
     *
     * @param str 内容
     * @return 编码后的内容
     */
    public static String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, LyCoreConstants.UTF8);
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    /**
     * 内容解码
     *
     * @param str 内容
     * @return 解码后的内容
     */
    public static String urlDecode(String str) {
        try {
            if (StringUtils.isNull(str)) {
                return "";
            }
            return URLDecoder.decode(str, LyCoreConstants.UTF8);
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    /**
     * 从请求中解析请求体
     */
    public static String resolveBodyFromRequest(HttpServletRequest request) throws IOException {
        StringBuilder sb = new StringBuilder("");
        ;
        try (BufferedReader reader = request.getReader()) {
            // 读取数据
            String line = null;
            if ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        }
        return sb.toString();
    }

    public static <T extends Resp<?>> void writeJson(T responseBody) throws IOException {
        HttpServletResponse response = getResponse();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try (PrintWriter out = response.getWriter()) {
            out.append(JSON.toJSONString(responseBody));
        }
    }

    public static <T extends Resp<?>> void writeHtml(T responseBody) throws Exception {
        HttpServletResponse response = getResponse();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=utf-8");
        try (PrintWriter out = response.getWriter()) {
            out.append(String.valueOf(responseBody.getData()));
        }
    }

    public static void writeHtml(String content) throws Exception {
        HttpServletResponse response = getResponse();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=utf-8");
        try (PrintWriter out = response.getWriter()) {
            out.append(content);
        }
    }

    /**
     * 判断是否ajax请求 spring ajax 返回含有 ResponseBody 或者 RestController注解
     *
     * @param handlerMethod HandlerMethod
     * @return 是否ajax请求
     */
    public boolean isBody(HandlerMethod handlerMethod) {
        ResponseBody responseBody = ClassUtils.getAnnotation(handlerMethod, ResponseBody.class);
        return responseBody != null;
    }

    /**
     * 读取cookie
     *
     * @param name cookie name
     * @return cookie value
     */
    public String getCookieVal(String name) {
        if (ServletUtils.getRequest().isPresent()) {
            return getCookieVal(ServletUtils.getRequest().get(), name);
        }
        return null;
    }

    /**
     * 读取cookie
     *
     * @param request HttpServletRequest
     * @param name    cookie name
     * @return cookie value
     */
    public String getCookieVal(HttpServletRequest request, String name) {
        Cookie cookie = getCookie(request, name);
        return cookie != null ? cookie.getValue() : null;
    }

    /**
     * 清除 某个指定的cookie
     *
     * @param response HttpServletResponse
     * @param key      cookie key
     */
    public void removeCookie(HttpServletResponse response, String key) {
        setCookie(response, key, null, 0);
    }

    /**
     * 设置cookie
     *
     * @param response        HttpServletResponse
     * @param name            cookie name
     * @param value           cookie value
     * @param maxAgeInSeconds maxage
     */
    public void setCookie(HttpServletResponse response, String name, String value, int maxAgeInSeconds) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setMaxAge(maxAgeInSeconds);
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
    }

    /**
     * 获取 HttpServletRequest
     *
     * @return {HttpServletRequest}
     */
    public Optional<HttpServletRequest> getRequest() {
        return Optional
                .ofNullable(RequestContextHolder.getRequestAttributes())
                .map((request) -> {
                    ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) request;
                    // 子线程共享
                    RequestContextHolder.setRequestAttributes(servletRequestAttributes, true);
                    return servletRequestAttributes.getRequest();
                });
    }

    public HttpServletRequest getRequestOfNonNull() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    /**
     * 获取 HttpServletResponse
     *
     * @return {HttpServletResponse}
     */
    public HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getResponse();
    }

    /**
     * 将URL的参数和body参数合并,如果是get请求是不需要获取body参数，只需要获取url参数
     *
     * @param request 请求
     * @return 请求参数, 包含body, url中参数
     */
    public static Map<String, Object> getAllParams(HttpServletRequest request) throws IOException {
        Map<String, Object> result = new HashMap<>(16);
        //获取URL上的参数
        Map<String, Object> urlParams = getUrlParams(request);
        result.putAll(urlParams);

        Map<String, Object> allRequestParam = getRequestBody(request);
        //将URL的参数和body参数进行合并
        if (allRequestParam != null) {
            result.putAll(allRequestParam);
        }
        return result;
    }

    /**
     * 获取 Body 参数
     *
     * @param request 请求
     * @return 请求body数据
     * @throws IOException 异常
     */
    public static Map<String, Object> getRequestBody(final HttpServletRequest request) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String str = "";
        StringBuilder wholeStr = new StringBuilder();
        //一行一行的读取body体里面的内容；
        while ((str = reader.readLine()) != null) {
            wholeStr.append(str);
        }
        //转化成json对象
        return (wholeStr.length() != 0) ?
                JSON.parseObject(wholeStr.toString(), new TypeReference<Map<String, Object>>() {
                }) :
                new HashMap<>(16);
    }

    /**
     * 将URL请求参数转换成Map
     *
     * @param request 请求
     * @return 返回url中所有参数
     */
    public static Map<String, Object> getUrlParams(final HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>(16);
        Enumeration<?> pNames = request.getParameterNames();
        while (pNames.hasMoreElements()) {
            String pName = (String) pNames.nextElement();
            String pValue = request.getParameter(pName);
            params.put(pName, pValue);
        }
        return params;
    }

    public static String getBodyString(ServletRequest request) {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        try (InputStream inputStream = request.getInputStream()) {
            reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String line = "";
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            log.warn("getBodyString出现问题！");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    log.error(ExceptionUtils.getMessage(e));
                }
            }
        }
        return sb.toString();
    }
}
