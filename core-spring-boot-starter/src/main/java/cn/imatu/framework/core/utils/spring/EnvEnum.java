package cn.imatu.framework.core.utils.spring;

import java.util.Arrays;

/**
 * @author shenguangyang
 */
public enum EnvEnum {
    // 开发环境
    DEV,
    // 日常环境
    DAILY,
    // 测试环境
    SIT, TEST,
    // 预发环境
    UAT, PRE,
    // 生产环境
    PROD,
    // 其他环境
    OTHER,

    ;
    public static EnvEnum ofByName(String profile) {
        String name = profile.toUpperCase();
        return Arrays.stream(values()).filter(e -> e.name().equals(name)).findFirst().orElse(OTHER);
    }
}
