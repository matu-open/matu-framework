package cn.imatu.framework.core.response;

import cn.imatu.framework.core.constant.WebType;

/**
 * @author shenguangyang
 */
public interface RequestHandler {
    /**
     * 通过key获取请求头
     */
    String getHeader(String key);

    WebType getWebType();
}
