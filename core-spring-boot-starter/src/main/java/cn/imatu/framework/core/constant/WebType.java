package cn.imatu.framework.core.constant;

/**
 * @author shenguangyang
 */
public enum WebType {
    ANY,
    SERVLET,
    REACTIVE;
}