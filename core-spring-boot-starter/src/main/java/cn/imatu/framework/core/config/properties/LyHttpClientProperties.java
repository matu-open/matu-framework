package cn.imatu.framework.core.config.properties;

import cn.imatu.framework.core.constant.LyCoreConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author shenguangyang
 */
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "http.client")
public class LyHttpClientProperties {
}
