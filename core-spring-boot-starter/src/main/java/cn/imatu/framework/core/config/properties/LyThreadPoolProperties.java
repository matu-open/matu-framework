package cn.imatu.framework.core.config.properties;

import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 线程池配置
 *
 * @author shenguangyang
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "thread-pool")
public class LyThreadPoolProperties {
    /**
     * 核心线程数
     */
    private Integer corePoolSize = Runtime.getRuntime().availableProcessors();
    /**
     * 最大线程数
     */
    private Integer maxPoolSize = Runtime.getRuntime().availableProcessors() * 2;
    /**
     * 队列数
     */
    private Integer queueCapacity = Runtime.getRuntime().availableProcessors() * 4;
    /* 当线程空闲时间达到keepAliveTime, 该线程会退出 */
    private Integer keepAliveSeconds = 60;
}
