package cn.imatu.framework.core.config;

import cn.imatu.framework.core.constant.LyCoreConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * Description:
 *
 * @author shenguangyang
 */
@Component
@Configuration
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "validator")
public class ValidatorProperties {
    /**
     * 获取添加枚举值，表示当传入的值没有在枚举中时，提示 message + 枚举中的值
     */
    private Boolean isAddEnumValue = true;

    public Boolean getIsAddEnumValue() {
        return isAddEnumValue;
    }

    public void setAddEnumValue(Boolean addEnumValue) {
        isAddEnumValue = addEnumValue;
    }
}
