package cn.imatu.framework.core.page;

import cn.imatu.framework.tool.core.StringUtils;
import cn.hutool.core.date.DateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 分页数据
 * @deprecated
 * @author shenguangyang
 */
@Getter
@Setter
@Slf4j
public class PageModel {
    /**
     * 当前记录起始索引
     */
    private Integer pageNum;

    /**
     * 每页显示记录数
     */
    private Integer pageSize;

    /**
     * 排序列
     */
    private String orderByColumn;

    /**
     * 开始时间
     */
    private DateTime beginDate;

    /**
     * 结束时间
     */
    private DateTime endDate;

    /**
     * 排序的方向desc或者asc
     */
    private String isAsc = "asc";

    public String getOrderBy() {
        if (StringUtils.isEmpty(orderByColumn)) {
            return "";
        }
        return StringUtils.humpToUnderline(orderByColumn) + " " + isAsc;
    }

    public void setIsAsc(String isAsc) {
        // 兼容前端排序类型
        if ("ascending".equals(isAsc)) {
            isAsc = "asc";
        } else if ("descending".equals(isAsc)) {
            isAsc = "desc";
        }
        this.isAsc = isAsc;
    }
}
