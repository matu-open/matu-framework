package cn.imatu.framework.core.http;

import cn.imatu.framework.core.constant.WebType;

/**
 * @author shenguangyang
 */
public interface HttpRequestManager {
    /**
     * 通过key获取请求头
     */
    String getHeader(String key);

    /**
     * 获取请求的uri
     */
    String getRequestUri();

    WebType getWebType();

    String getRequestIp();

    String getRemoteAddress();
}
