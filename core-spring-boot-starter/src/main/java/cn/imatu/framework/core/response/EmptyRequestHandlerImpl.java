package cn.imatu.framework.core.response;

import cn.imatu.framework.core.constant.WebType;

/**
 * @author shenguangyang
 */
public class EmptyRequestHandlerImpl implements RequestHandler {

    @Override
    public String getHeader(String key) {
        return "";
    }

    @Override
    public WebType getWebType() {
        return null;
    }
}
