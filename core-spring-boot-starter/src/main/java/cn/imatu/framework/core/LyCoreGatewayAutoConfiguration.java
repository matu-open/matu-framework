package cn.imatu.framework.core;


import cn.imatu.framework.core.response.ReactiveRequestHandlerImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Slf4j
@AutoConfiguration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
@Import({
        ReactiveRequestHandlerImpl.class
})
public class LyCoreGatewayAutoConfiguration {
    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
