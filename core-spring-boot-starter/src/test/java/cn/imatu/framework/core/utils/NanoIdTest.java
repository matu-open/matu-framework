package cn.imatu.framework.core.utils;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.lang.id.NanoId;

/**
 * @author shenguangyang
 * @date 2022-03-31 7:43
 */
public class NanoIdTest {
    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();
            System.out.println("randomNanoId = " + NanoId.randomNanoId() + "\t, time: " + (System.currentTimeMillis() - start));
            start = System.currentTimeMillis();
            System.out.println("randomUUID = " + UUID.randomUUID() + "\t, time: " + (System.currentTimeMillis() - start));
        }
    }
}
