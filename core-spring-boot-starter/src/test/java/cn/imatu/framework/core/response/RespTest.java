package cn.imatu.framework.core.response;

import lombok.Data;

/**
 * @author shenguangyang
 */
class RespTest {
    @Data
    public static class TestData {
        private String name = "xiaohong";
        private String username = "admin";
        private String address = "中国";
    }

}