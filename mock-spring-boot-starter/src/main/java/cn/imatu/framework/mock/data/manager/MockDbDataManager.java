package cn.imatu.framework.mock.data.manager;

import cn.imatu.framework.mock.data.model.MysqlTable;
import cn.imatu.framework.mock.data.model.MysqlTableColumn;

import java.util.List;

/**
 * 模拟数据库中数据
 *
 * @author shenguangyang
 */
public interface MockDbDataManager {
    /**
     * 获取数据库中所有表
     */
    List<MysqlTable> listDbTable();

    /**
     * 获取某个表中所有字段信息
     *
     * @param tableName 表明
     */
    List<MysqlTableColumn> listDbTableColumn(String tableName);

    /**
     * 生成数据并且插入到数据
     *
     * @param num 生成的数量
     */
    int mockDataAndSave(String tableName, int num);

    /**
     * 创建 模拟数据生成器代码
     *
     * @param tableName 表名
     */
    String createMockDataGeneratorCode(String tableName);
}
