package cn.imatu.framework.mock.data.generator.impl;

import cn.imatu.framework.mock.data.generator.MockDataRule;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @author shenguangyang
 */
public class DefaultMockDataRuleImpl implements MockDataRule {
    @Override
    public Map<Supplier<String>, String[]> ruleData() {
        Map<Supplier<String>, String[]> resp = new HashMap<>();
        resp.put(() -> "1", new String[]{"*"});
        return resp;
    }

}
