package cn.imatu.framework.mock.data.model;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * 数据库中表字段信息
 *
 * @author shenguangyang
 */
@Getter
@Setter
public class MysqlTableColumn {
    @JSONField(name = "TABLE_CATALOG")
    private String tableCatalog;

    @JSONField(name = "IS_NULLABLE")
    private String isNullable;

    @JSONField(name = "TABLE_NAME")
    private String tableName;

    @JSONField(name = "TABLE_SCHEMA")
    private String tableSchema;

    @JSONField(name = "EXTRA")
    private String extra;

    @JSONField(name = "COLUMN_NAME")
    private String columnName;

    @JSONField(name = "COLUMN_KEY")
    private String columnKey;

    @JSONField(name = "CHARACTER_OCTET_LENGTH")
    private Integer characterOctetLength;

    @JSONField(name = "PRIVILEGES")
    private String privileges;

    @JSONField(name = "COLUMN_COMMENT")
    private String columnComment;

    @JSONField(name = "COLLATION_NAME")
    private String collationName;

    @JSONField(name = "COLUMN_TYPE")
    private String columnType;

    @JSONField(name = "GENERATION_EXPRESSION")
    private String generationExpression;

    @JSONField(name = "ORDINAL_POSITION")
    private String ordinalPosition;

    @JSONField(name = "CHARACTER_MAXIMUM_LENGTH")
    private String characterMaximumLength;

    @JSONField(name = "DATA_TYPE")
    private String dataType;

    @JSONField(name = "COLUMN_DEFAULT")
    private String columnDefault;

    @JSONField(name = "CHARACTER_SET_NAME")
    private String characterSetName;

    @JSONField(name = "NUMERIC_PRECISION")
    private String numericPrecision;

    @JSONField(name = "NUMERIC_SCALE")
    private String numericScale;


    public static List<MysqlTableColumn> from(List<Map<String, Object>> dbData) {
        String jsonData = JSON.toJSONString(dbData);
        return JSON.parseObject(jsonData, new TypeReference<List<MysqlTableColumn>>() {
        });
    }
}
