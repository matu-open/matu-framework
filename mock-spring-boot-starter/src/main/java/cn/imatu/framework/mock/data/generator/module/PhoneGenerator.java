package cn.imatu.framework.mock.data.generator.module;

import lombok.Getter;

import java.util.Random;

/**
 * 手机号生成器
 *
 * @author shenguangyang
 */
public class PhoneGenerator {
    //中国移动
    public static final String[] CHINA_MOBILE = {
            "134", "135", "136", "137", "138", "139", "150", "151", "152", "157", "158", "159",
            "182", "183", "184", "187", "188", "178", "147", "172", "198"
    };
    //中国联通
    public static final String[] CHINA_UNICOM = {
            "130", "131", "132", "145", "155", "156", "166", "171", "175", "176", "185", "186", "166"
    };
    //中国电信
    public static final String[] CHINA_TELECOME = {
            "133", "149", "153", "173", "177", "180", "181", "189", "199"
    };

    @Getter
    public enum Operator {
        //中国移动
        MOBILE(0),
        //中国联通
        UNICOM(1),
        //中国电信
        TELECOME(2);
        private final int op;

        Operator(int op) {
            this.op = op;
        }
    }

    public static String createMobile() {
        // 随机运营商标志位
        Random random = new Random();
        int op = random.nextInt(3);
        return createMobile(Operator.values()[op]);
    }

    /**
     * 生成手机号
     *
     * @param op 0 移动 1 联通 2 电信
     */
    public static String createMobile(Operator op) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        String mobile01;// 手机号前三位
        int temp;
        if (Operator.MOBILE.equals(op)) {
            mobile01 = CHINA_MOBILE[random.nextInt(CHINA_MOBILE.length)];
        } else if (Operator.UNICOM.equals(op)) {
            mobile01 = CHINA_UNICOM[random.nextInt(CHINA_UNICOM.length)];
        } else if (Operator.TELECOME.equals(op)) {
            mobile01 = CHINA_TELECOME[random.nextInt(CHINA_TELECOME.length)];
        } else {
            mobile01 = "op标志位有误！";
        }
        if (mobile01.length() > 3) {
            return mobile01;
        }
        sb.append(mobile01);
        // 生成手机号后8位
        for (int i = 0; i < 8; i++) {
            temp = random.nextInt(10);
            sb.append(temp);
        }
        return sb.toString();
    }
}
