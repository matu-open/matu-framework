package cn.imatu.framework.mock.data.model;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * 数据库中表信息
 *
 * @author shenguangyang
 */
@Getter
@Setter
public class MysqlTable {
    @JSONField(name = "TABLE_CATALOG")
    private String tableCatalog;

    @JSONField(name = "TABLE_COMMENT")
    private String tableComment;

    @JSONField(name = "TABLE_NAME")
    private String tableName;

    @JSONField(name = "TABLE_SCHEMA")
    private String tableSchema;

    @JSONField(name = "ENGINE")
    private String engine;

    @JSONField(name = "TABLE_TYPE")
    private String tableType;

    @JSONField(name = "TABLE_ROWS")
    private Long tableRows;

    @JSONField(name = "AVG_ROW_LENGTH")
    private Long avgRowLength;

    @JSONField(name = "DATA_LENGTH")
    private Long dataLength;

    @JSONField(name = "DATA_FREE")
    private Long dataFree;

    @JSONField(name = "INDEX_LENGTH")
    private Integer indexLength;

    @JSONField(name = "ROW_FORMAT")
    private String rowFormat;

    @JSONField(name = "VERSION")
    private Integer version;

    @JSONField(name = "CREATE_OPTIONS")
    private String createOptions;

    @JSONField(name = "CREATE_TIME")
    private Long createTime;

    @JSONField(name = "MAX_DATA_LENGTH")
    private Long maxDataLength;

    @JSONField(name = "TABLE_COLLATION")
    private String tableCollation;

    public static List<MysqlTable> from(List<Map<String, Object>> dbData) {
        String jsonData = JSON.toJSONString(dbData);
        return JSON.parseObject(jsonData, new TypeReference<List<MysqlTable>>() {
        });
    }
}
