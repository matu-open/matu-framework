package cn.imatu.framework.mock.data.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 模拟数据mapper
 *
 * @author shenguangyang
 */
public interface MockDbDataMapper {
    @Select("select * from information_schema.TABLES where TABLE_SCHEMA=(select database())")
    List<Map<String, Object>> listTable();

    /**
     * 获取表信息
     *
     * @param tableName 表名
     * @return 表行数据
     */
    @Select("select * from information_schema.COLUMNS where TABLE_SCHEMA = (select database()) and TABLE_NAME=#{tableName}")
    List<Map<String, Object>> listTableColumn(String tableName);

    /**
     * 批量插入数据
     */
    int batchInsert(@Param("tableName") String tableName, @Param("columnNameList") List<String> columnNameList,
                    @Param("columnDataList") List<List<Object>> columnDataList);
}
