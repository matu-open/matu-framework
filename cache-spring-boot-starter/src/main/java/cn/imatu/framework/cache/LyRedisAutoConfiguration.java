package cn.imatu.framework.cache;

import cn.imatu.framework.cache.redis.bloom_filter.RedisBloomFilter;
import cn.imatu.framework.cache.redis.bloom_filter.RedisBloomFilterProperties;
import cn.imatu.framework.cache.redis.config.RedisConfig;
import cn.imatu.framework.cache.redis.service.RedisHashOps;
import cn.imatu.framework.cache.redis.service.RedisKeyOps;
import cn.imatu.framework.cache.redis.service.RedisListOps;
import cn.imatu.framework.cache.redis.service.RedisService;
import cn.imatu.framework.cache.redis.service.RedisValueOps;
import cn.imatu.framework.cache.redis.utils.RedisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * 过期回调函数, 需要继承 {@link org.springframework.data.redis.listener.KeyExpirationEventMessageListener}
 * 实现的方法如下:
 * <code>
 * public void onMessage(Message message, byte[] pattern) {
 * System.out.println(new String(message.getBody()));
 * System.out.println(new String(message.getChannel()));
 * System.out.println(new String(pattern));
 * super.onMessage(message, pattern);
 * }
 * </code>
 *
 * @author shenguangyang
 */
@AutoConfiguration
@ConditionalOnClass(RedisAutoConfiguration.class)
@AutoConfigureBefore(RedisAutoConfiguration.class)
@EnableConfigurationProperties({RedisBloomFilterProperties.class})
@EnableCaching
@Import({
        RedisUtils.class, RedisHashOps.class, RedisKeyOps.class, RedisService.class,
        RedisListOps.class, RedisValueOps.class, RedisBloomFilter.class
})
@ImportAutoConfiguration(RedisConfig.class)
public class LyRedisAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyRedisAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
