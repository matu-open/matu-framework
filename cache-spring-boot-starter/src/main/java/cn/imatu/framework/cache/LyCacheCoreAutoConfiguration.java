package cn.imatu.framework.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
public class LyCacheCoreAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyCacheCoreAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
