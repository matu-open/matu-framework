package cn.imatu.framework.cache.redis.bloom_filter;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author shenguangyang
 */
@ConfigurationProperties("bloom.filter")
public class RedisBloomFilterProperties {
    /**
     * 预计数据总量
     */
    private long expectedInsertions;
    /**
     * 容错率
     */
    private double fpp;

    public long getExpectedInsertions() {
        return expectedInsertions;
    }

    public void setExpectedInsertions(long expectedInsertions) {
        this.expectedInsertions = expectedInsertions;
    }

    public double getFpp() {
        return fpp;
    }

    public void setFpp(double fpp) {
        this.fpp = fpp;
    }
}
