package cn.imatu.framework.cache.redis.config;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONReader;
import org.junit.jupiter.api.Test;

/**
 * @author shenguangyang
 */
class FastJson2JsonRedisSerializerTest {
    public static class Demo {
        private String name;
        private String age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }
    }

    @Test
    public void test() {
        Demo demo = new Demo();
        demo.setAge("11");
        demo.setName("11");

        String s = JSON.toJSONString(demo);
        Demo demo2 = (Demo) JSON.parse(s, JSONReader.Feature.SupportAutoType);
        System.out.println("end");
    }

}