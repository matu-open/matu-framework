package cn.imatu.framework.cache.core.key;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.concurrent.TimeUnit;

/**
 * @author shenguangyang
 */
@AllArgsConstructor
@Getter
public enum DefaultCacheKey implements ICacheKey {
    TEST("login_tokens:${userId}:${tenantId}", 720, TimeUnit.MINUTES),
    ;

    private final String key;
    private final int expire;
    private final TimeUnit unit;

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public int getExpire() {
        return expire;
    }

    @Override
    public TimeUnit getUnit() {
        return unit;
    }
}
