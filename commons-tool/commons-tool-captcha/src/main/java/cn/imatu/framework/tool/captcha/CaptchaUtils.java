package cn.imatu.framework.tool.captcha;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

/**
 * 验证码生产工具，生产一个图片
 *
 * @author shenguangyang
 */
public class CaptchaUtils {
    /**
     * 宽度
     */
    private static final int CAPTCHA_WIDTH = 100;
    /**
     * 高度
     */
    private static final int CAPTCHA_HEIGHT = 35;
    /**
     * 数字的长度
     */
    private int numberCnt = 4;
    /**
     * 图片类型
     */
    private static final String IMAGE_TYPE = "JPEG";

    private final Random r = new Random();
    /**
     * 字体
     */
    private final String[] fontNames = {"宋体", "黑体", "微软雅黑"};

    /**
     * 可选字符
     */
    private String codes = "23456789abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";

    /**
     * 背景色,白色
     */
    private final Color bgColor = new Color(255, 255, 255);

    /**
     * 验证码上的文本
     */
    private String text;

    private volatile static CaptchaUtils utils = null;

    /**
     * @param numberCnt 生成的字符数
     */
    private CaptchaUtils(int numberCnt) {
        this.numberCnt = numberCnt;
    }

    /**
     * 实例化对象
     *
     * @return this
     */
    public static CaptchaUtils getInstance(int numberCnt) {
        if (utils == null) {
            synchronized (CaptchaUtils.class) {
                if (utils == null) {
                    utils = new CaptchaUtils(numberCnt);
                }
            }
        }
        return utils;
    }

    /**
     * 创建验证码
     *
     * @param path 路径地址
     * @return 验证码
     * @throws Exception 异常
     */
    public String getCode(String path) throws Exception {
        BufferedImage bi = utils.getImage();
        output(bi, Files.newOutputStream(Paths.get(path)));
        return this.text;
    }

    /**
     * 机能概要:生成图片对象，并返回
     *
     * @return 验证码
     * @throws Exception 异常
     */
    public Captcha getCode() throws Exception {
        BufferedImage img = utils.getImage();

        //返回验证码对象
        Captcha code = new Captcha();
        code.setText(this.text);
        code.setData(this.copyImage2Byte(img));
        return code;
    }

    /**
     * 机能概要:将图片转化为 二进制数据
     *
     * @param img 图片
     * @return 字节数组
     * @throws Exception 异常
     */
    public byte[] copyImage2Byte(BufferedImage img) throws Exception {
        //字节码输出流
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        //写数据到输出流中
        ImageIO.write(img, IMAGE_TYPE, bout);

        //返回数据
        return bout.toByteArray();
    }

    /**
     * 机能概要:将二进制数据转化为文件
     *
     * @param data
     * @param file
     * @throws Exception
     */
    public boolean copyByte2File(byte[] data, String file) throws Exception {
        try (ByteArrayInputStream in = new ByteArrayInputStream(data); FileOutputStream out = new FileOutputStream(file)) {
            byte[] buff = new byte[1024];
            int len = 0;
            while ((len = in.read(buff)) > -1) {
                out.write(buff, 0, len);
            }
            out.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 机能概要:生成随机的颜色
     *
     * @return
     */
    private Color randomColor() {
        int red = r.nextInt(150);
        int green = r.nextInt(150);
        int blue = r.nextInt(150);
        return new Color(red, green, blue);
    }

    /**
     * 机能概要:生成随机的字体
     *
     * @return
     */
    private Font randomFont() {
        int index = r.nextInt(fontNames.length);
        // 生成随机的字体名称
        String fontName = fontNames[index];
        // 生成随机的样式, 0(无样式), 1(粗体), 2(斜体), 3(粗体+斜体)
        int style = r.nextInt(4);
        // 生成随机字号, 24 ~ 28
        int size = r.nextInt(5) + 24;
        return new Font(fontName, style, size);
    }

    /**
     * 画干扰线
     *
     * @param image
     */
    private void drawLine(BufferedImage image) {
        // 一共画5条
        int num = 5;
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        // 生成两个点的坐标，即4个值
        for (int i = 0; i < num; i++) {
            int x1 = r.nextInt(CAPTCHA_WIDTH);
            int y1 = r.nextInt(CAPTCHA_HEIGHT);
            int x2 = r.nextInt(CAPTCHA_WIDTH);
            int y2 = r.nextInt(CAPTCHA_HEIGHT);
            g2.setStroke(new BasicStroke(1.5F));
            // 随机生成干扰线颜色
            g2.setColor(randomColor());
            // 画线
            g2.drawLine(x1, y1, x2, y2);
        }
    }

    /**
     * 随机生成一个字符
     *
     * @return
     */
    private char randomChar() {
        int index = r.nextInt(codes.length());
        return codes.charAt(index);
    }

    /**
     * 创建BufferedImage
     *
     * @return
     */
    private BufferedImage createImage() {
        BufferedImage image = new BufferedImage(CAPTCHA_WIDTH, CAPTCHA_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        g2.setColor(this.bgColor);
        g2.fillRect(0, 0, CAPTCHA_WIDTH, CAPTCHA_HEIGHT);
        return image;
    }

    /**
     * 调用这个方法得到验证码
     *
     * @return 图片
     */
    public BufferedImage getImage() {
        // 创建图片缓冲区
        BufferedImage image = createImage();
        // 得到绘制环境
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        // 用来装载生成的验证码文本
        StringBuilder sb = new StringBuilder();
        // 向图片中画4个字符
        // 循环四次，每次生成一个字符
        for (int i = 0; i < numberCnt; i++) {
            // 随机生成一个字母
            String s = randomChar() + "";
            // 把字母添加到sb中
            sb.append(s);
            // 设置当前字符的x轴坐标
            float x = i * 1.0F * CAPTCHA_WIDTH / numberCnt;
            // 设置随机字体
            g2.setFont(randomFont());
            // 设置随机颜色
            g2.setColor(randomColor());
            // 画图
            g2.drawString(s, x, CAPTCHA_HEIGHT - 5);
        }
        // 把生成的字符串赋给了this.text
        this.text = sb.toString();
        // 添加干扰线
        drawLine(image);
        return image;
    }

    /**
     * 机能概要:
     *
     * @return 返回验证码图片上的文本
     */
    public String getText() {
        return text;
    }

    // 保存图片到指定的输出流
    public static void output(BufferedImage image, OutputStream out) throws IOException {
        ImageIO.write(image, IMAGE_TYPE, out);
    }

    //图片验证码对象
    public static class Captcha {
        //验证码文字信息
        private String text;
        //验证码二进制数据
        private byte[] data;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public byte[] getData() {
            return data;
        }

        public void setData(byte[] data) {
            this.data = data;
        }
    }
}
