package cn.imatu.framework.tool.core.jar.test;

import cn.imatu.framework.tool.core.jar.ResourcesInfo;

import java.util.HashSet;
import java.util.Set;

/**
 * @author shenguangyang
 */
public class TestDataResourcesInfo implements ResourcesInfo {
    @Override
    public Set<String> classPaths() {
        return new HashSet<String>() {{
            add("");
        }};
    }
}
