package cn.imatu.framework.tool.core.tree;

import java.io.Serializable;
import java.util.List;

/**
 * 如果想要设置每个节点的所有父节点id(treeParentIds), {@link #treeParentIds(String)} 方法必须实现
 * 如果想要设置每个节点的所有父节名称(treeNames), {@link #treeNames(String)} 方法必须实现
 * 如果想要设置每个节点的等级(treeLevel), {@link #treeLevel(Integer)} 方法必须实现
 *
 * @author shenguangyang
 */
public interface TreeNode<ID, E> extends Serializable {
    /**
     * 树的主键
     */
    ID treeId();

    void treeId(ID treeId);

    /**
     * 树的父id
     */
    ID treeParentId();

    void treeParentId(ID parentId);

    /**
     * 当前节点的所有子节点
     */
    List<E> children();

    /**
     * 获取树的名字
     */
    String name();

    /**
     * 设置子节点
     */
    void children(List<E> children);

    /**
     * 在当前节点所在层的排序
     */
    default Integer sortNo() {
        return 0;
    }

    /**
     * 是否为树叶
     */
    default void treeLeaf(Boolean treeLeaf) {

    }

    /**
     * 树的层次
     */
    default void treeLevel(Integer treeLevel) {

    }

    /**
     * 设置树的名称, eg: 北京市/北京城区/东城区
     */
    default void treeNames(String names) {

    }

    /**
     * 设置树的父节点id, 默认使用逗号隔离, eg: 1,2,3,4
     */
    default void treeParentIds(String parentIds) {

    }
}
