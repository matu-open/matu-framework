package cn.imatu.framework.tool.core;

import cn.imatu.framework.tool.core.exception.UtilException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * sql操作工具类
 *
 * @author shenguangyang
 */
public class SqlUtils {
    /**
     * 仅支持字母、数字、下划线、空格、逗号、小数点（支持多个字段排序）
     */
    public static String SQL_PATTERN = "[a-zA-Z0-9_\\ \\,\\.]+";

    public static Pattern sqlInjectionPattern = Pattern.compile("\\b(and|exec|insert|select|drop|grant|alter|delete|update|count|chr|mid|master|truncate|char|declare|or)\\b|(\\+|\"|%)");

    /**
     * 检查字符，防止注入绕过
     */
    public static String escapeOrderBySql(String value) {
        if (StringUtils.isNotEmpty(value) && !isValidOrderBySql(value)) {
            throw new UtilException("参数不符合规范，不能进行查询");
        }
        return value;
    }

    /**
     * 验证 order by 语法是否符合规范
     */
    public static boolean isValidOrderBySql(String value) {
        return value.matches(SQL_PATTERN);
    }


    /**
     * 是否含有sql注入，返回true表示含有
     *
     * @param str 字符串
     * @return boolean
     */
    public static boolean containsSqlInjection(String str) {
        Matcher matcher = sqlInjectionPattern.matcher(str);
        return matcher.find();
    }
}
