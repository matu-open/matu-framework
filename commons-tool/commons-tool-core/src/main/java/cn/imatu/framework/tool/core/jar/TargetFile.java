package cn.imatu.framework.tool.core.jar;

import cn.imatu.framework.tool.core.StringUtils;

import java.io.File;

/**
 * 将jar包中的resources目录下文件拷贝到主机上的文件信息
 *
 * @author shenguangyang
 */
public class TargetFile {
    /**
     * 完整路径
     */
    private final String fullPath;
    /**
     * 目标文件名称
     */
    private String classPath;
    /**
     * 目标目录路径
     * eg: /root/a/b
     */
    private String dirPath;

    public TargetFile(String classPath, CopyResourcesInfo copyResourcesInfo) {
        this.classPath = classPath;
        // 判断用户是否指定了自定义目录
        String customDirPath = copyResourcesInfo.getCustomDirPath();
        String dirName = copyResourcesInfo.getDirName();
        if (StringUtils.isEmpty(customDirPath)) {
            this.dirPath = JarResourcesCopyConstant.LIB_CACHE_PATH + File.separator + dirName;
        } else {
            this.dirPath = customDirPath + File.separator + dirName;
        }
        this.fullPath = this.dirPath + File.separator + this.classPath;

        File file = new File(this.fullPath);
        if (!file.getParentFile().exists()) {
            if (!file.getParentFile().mkdirs()) {
                throw new RuntimeException("create dir fail, dirPath: " + file.getParentFile().getPath());
            }
        }
    }

    public String getFullPath() {
        return fullPath;
    }

    public String getClassPath() {
        return classPath;
    }

    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }

    public String getDirPath() {
        return dirPath;
    }

    public void setDirPath(String dirPath) {
        this.dirPath = dirPath;
    }
}
