package cn.imatu.framework.tool.core.jar;

import java.io.File;
import java.io.IOException;

/**
 * 打包jar的时候将lib提取到了外部
 *
 * @author shenguangyang
 */
public class JarBuildByLoaderExtractLib extends JarBuildWay {
    private final String loaderPath;

    public JarBuildByLoaderExtractLib(String loaderPath, CopyResourcesInfo copyResourcesInfo) {
        super(copyResourcesInfo);
        this.loaderPath = loaderPath.replace("\\", File.separator);
    }

    @Override
    protected void doCopyResourcesToLocal() throws IOException {

    }
}
