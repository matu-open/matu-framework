package cn.imatu.framework.tool.core.jar;

import cn.imatu.framework.tool.core.RegexUtils;
import cn.imatu.framework.tool.core.StringUtils;
import cn.imatu.framework.tool.core.UrlUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.function.Function;

/**
 * @author shenguangyang
 */
public class JarResourcesCopyHandler {
    private final JarBuildWay jarBuildWay;

    private JarResourcesCopyHandler(JarBuildWay jarBuildWay) {
        this.jarBuildWay = jarBuildWay;
    }

    public static JarResourcesCopyHandler create(CopyResourcesInfo copyResourcesInfo) throws Exception {
        JarBuildWay jarBuildWay = JarBuildWayFactory.create(copyResourcesInfo);
        return new JarResourcesCopyHandler(jarBuildWay);
    }

    public JarResourcesCopyHandler addCopyAfterCallback(Function<JarResourcesFile, Boolean> copyAfterCallback) {
        this.jarBuildWay.copyAfterCallback = copyAfterCallback;
        return this;
    }

    /**
     * 获取拷贝的目标路径, 不包含资源路径
     * 比如你的资源路径是 test/config.yaml, 内部随机生成的目录路径是 /aaa/bbb/ccc
     * 则返回的是 /aaa/bbb/ccc
     * 文件的完整路径其实是 /aaa/bbb/ccc/test/config.yaml
     */
    public String getTargetDirPath() throws UnsupportedEncodingException {
        return URLDecoder.decode(this.jarBuildWay.targetDirPath, "utf-8");
    }

    /**
     * 添加排除的文件, 内部在拷贝资源时候, 已经将src/main/java目录下的.class文件都过滤掉了
     * 但是, 如果在src/main/java存放非java源文件(比如com/test/a.xml), 且指定拷贝的资源路径是 "",
     * 那会将com/test/a.xml也进行了拷贝, 若不想拷贝, 直接指定排除的文件, 即 com/test/*.xml
     * <p>
     * 同样, 你不想要拷贝 resources 资源下的某些文件, 也可以这么干, 比如我不想要resources/test/readme.md
     * 文件, 则可以指定排除的路径为 test/readme.md
     * eg:
     * `**`/target
     * logs
     * .git
     * `**`/*.md
     * <p>
     * * 表示匹配0或多个不是/的字符
     * ** 表示匹配0或多个任意字符
     * ? 表示匹配1个任意字符
     */
    public JarResourcesCopyHandler addExcludeFile(String excludeFile) {
        if (StringUtils.isEmpty(excludeFile)) {
            return this;
        }
        // 去除前缀的干扰
        excludeFile = "**/" + UrlUtils.removeStartsSlash(excludeFile);
        excludeFile = excludeFile.replace("\\", "/");
        String regPath = RegexUtils.getRegex(excludeFile);
        this.jarBuildWay.copyExcludeFiles.add(regPath);
        return this;
    }

    /**
     * 执行, 主逻辑
     */
    public JarResourcesCopyHandler exec() throws IOException {
        this.jarBuildWay.exec();
        return this;
    }
}
