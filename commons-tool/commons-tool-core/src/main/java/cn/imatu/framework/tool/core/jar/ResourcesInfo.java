package cn.imatu.framework.tool.core.jar;

import java.util.HashSet;
import java.util.Set;

/**
 * 资源信息
 *
 * @author shenguangyang
 */
public interface ResourcesInfo {
    /**
     * 资源路径信息
     * 路径只能以/分割且不能以/开头
     */
    default Set<String> classPaths() {
        return new HashSet<String>() {{
            add("");
        }};
    }
}
