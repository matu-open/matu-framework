package cn.imatu.framework.tool.core;

/**
 * 去除文章内容页页面代码里的HTML标签
 *
 * @author shenguangyang
 */
public class HtmlUtils {
    /**
     * 去除html代码中含有的标签
     *
     * @param htmlStr html字符串
     * @return 被删除tag的html数据
     */
    public static String delTags(String htmlStr) {
        //定义script的正则表达式，去除js可以防止注入
        String scriptRegex = "<script[^>]*?>[\\s\\S]*?<\\/script>";
        //定义style的正则表达式，去除style样式，防止css代码过多时只截取到css样式代码
        String styleRegex = "<style[^>]*?>[\\s\\S]*?<\\/style>";
        //定义HTML标签的正则表达式，去除标签，只提取文字内容
        String htmlRegex = "<[^>]+>";
        //定义空格,回车,换行符,制表符
        String spaceRegex = "\\s*|\t|\r|\n";

        // 过滤script标签
        htmlStr = htmlStr.replaceAll(scriptRegex, "");
        // 过滤style标签
        htmlStr = htmlStr.replaceAll(styleRegex, "");
        // 过滤html标签
        htmlStr = htmlStr.replaceAll(htmlRegex, "");
        // 过滤空格等
        htmlStr = htmlStr.replaceAll(spaceRegex, "");
        // 返回文本字符串
        return htmlStr.trim();
    }

    /**
     * 获取HTML代码里的内容 去除所有空格
     *
     * @param htmlStr html字符串
     * @return 去除所有空格的html
     */
    public static String getTextFromHtml(String htmlStr) {
        //去除html标签
        htmlStr = delTags(htmlStr);
        //去除空格" "
        htmlStr = htmlStr.replaceAll(" ", "");
        return htmlStr;
    }
}
