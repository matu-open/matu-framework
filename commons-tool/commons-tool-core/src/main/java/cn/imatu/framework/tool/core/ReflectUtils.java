package cn.imatu.framework.tool.core;

import cn.hutool.core.convert.Convert;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 反射工具类. 提供调用getter/setter方法, 访问私有变量, 调用私有方法, 获取泛型类型Class, 被AOP过的真实类等工具函数.
 *
 * @author shenguangyang
 */
@SuppressWarnings("unchecked")
public class ReflectUtils {
    public final static Pattern GROUP_VAR = Pattern.compile("\\$(\\d+)");

    /**
     * 正则中需要被转义的关键字
     */
    public final static Set<Character> RE_KEYS = new HashSet<>(
            Arrays.asList('$', '(', ')', '*', '+', '.', '[', ']', '?', '\\', '^', '{', '}', '|'));
    ;

    private static final String SETTER_PREFIX = "set";

    private static final String GETTER_PREFIX = "get";

    private static final String CGLIB_CLASS_SEPARATOR = "$$";

    private static final Logger logger = LoggerFactory.getLogger(ReflectUtils.class);

    /**
     * 缓存对象字段, key = 类名
     */
    private static final Map<String, List<Field>> beanFieldsCache = new ConcurrentHashMap<>();
    private static final Map<String, Field> beanSingleFieldCache = new ConcurrentHashMap<>();

    /**
     * 获取bean的所有字段
     *
     * @param object 对象
     * @return 类中所有字段信息, 不包含父类
     */
    public static List<Field> getFields(Object object) {
        return getFields(object.getClass());
    }

    /**
     * 获取对象的某个字段
     * @param entity 实体类
     * @param fieldName 字段名称
     */
    public static Field getField(Class<?> entity, String fieldName) {
        String key = entity.getName() + ":" + fieldName;
        Field field = beanSingleFieldCache.get(key);
        if (Objects.isNull(field)) {
            getFieldsWithSuper(entity);
        }
        return beanSingleFieldCache.get(key);
    }

    public static Field getField(Object entity, String fieldName) {
        if (Objects.isNull(entity)) {
            return null;
        }
        return getField(entity.getClass(), fieldName);
    }

    /**
     * 获取bean的所有字段 (包括父类字段)
     *
     * @param targetClass 目标类
     * @return 类中所有字段信息, 不包含父类
     */
    public static List<Field> getFields(Class<?> targetClass) {
        String name = targetClass.getName();
        List<Field> fields = beanFieldsCache.get(name);
        if (fields == null) {
            synchronized (name.intern()) {
                fields = beanFieldsCache.get(name);
                if (fields == null) {
                    fields = new ArrayList<>();
                    Field[] fieldData = targetClass.getDeclaredFields();
                    for (Field field : fieldData) {
                        field.setAccessible(true);
                        fields.add(field);
                        beanSingleFieldCache.put(name + ":" + field.getName(), field);
                    }
                    beanFieldsCache.put(name, fields);
                }
            }
        }
        return fields;
    }

    /**
     * 获取bean的所有字段 (包括父类字段)
     *
     * @param object 对象
     * @return 类中所有字段信息, 包含父类
     */
    public static List<Field> getFieldsWithSuper(Object object) {
        return getFieldsWithSuper(object.getClass());
    }

    /**
     * 获取bean的所有字段 (包括父类字段)
     *
     * @param targetClass 目标类
     * @return 获取类中所有字段信息, 包含父类
     */
    public static List<Field> getFieldsWithSuper(Class<?> targetClass) {
        String name = targetClass.getName();
        List<Field> fields = beanFieldsCache.get(name);
        if (fields == null) {
            synchronized (name.intern()) {
                fields = beanFieldsCache.get(name);
                if (fields == null) {
                    fields = new ArrayList<>();
                    while (targetClass != null) {
                        Field[] fieldData = targetClass.getDeclaredFields();
                        for (Field field : fieldData) {
                            field.setAccessible(true);
                            fields.add(field);
                            beanSingleFieldCache.put(name + ":" + field.getName(), field);
                        }
                        targetClass = targetClass.getSuperclass();
                    }
                    beanFieldsCache.put(name, fields);
                }
            }
        }
        return fields;
    }

    /**
     * 获取某个类的所有静态属性
     *
     * @param tClass 类
     * @return key: 类静态属性名, value: 类静态属性值
     * @throws IllegalAccessException 异常
     */
    public static <T> Map<String, Object> getAllStaticField(Class<T> tClass) throws IllegalAccessException {
        Field[] declaredFields = tClass.getDeclaredFields();
        Map<String, Object> result = new HashMap<>();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            if (Modifier.isStatic(field.getModifiers())) {
                result.put(field.getName(), field.get(tClass));
            }
        }
        return result;
    }

    /**
     * 调用Getter方法.
     * 支持多级，如：对象名.对象名.方法
     *
     * @param obj          对象
     * @param propertyName 属性名
     * @return get方法返回值
     * @throws NoSuchMethodException 异常
     */
    @SuppressWarnings("unchecked")
    public static <E> E invokeGetter(Object obj, String propertyName) throws NoSuchMethodException {
        Object object = obj;
        for (String name : StringUtils.split(propertyName, ".")) {
            String getterMethodName = GETTER_PREFIX + StringUtils.capitalize(name);
            object = invokeMethod(object, getterMethodName, new Class[]{}, new Object[]{});
        }
        return (E) object;
    }

    /**
     * 调用Setter方法, 仅匹配方法名。
     * 支持多级，如：对象名.对象名.方法
     *
     * @param obj          对象
     * @param propertyName 属性名
     * @param value        想要set的数据
     * @throws NoSuchMethodException 异常
     */
    public static <E> void invokeSetter(Object obj, String propertyName, E value) throws NoSuchMethodException {
        Object object = obj;
        String[] names = StringUtils.split(propertyName, ".");
        for (int i = 0; i < names.length; i++) {
            if (i < names.length - 1) {
                String getterMethodName = GETTER_PREFIX + StringUtils.capitalize(names[i]);
                try {
                    object = invokeMethod(object, getterMethodName, new Class[]{}, new Object[]{});
                } catch (NoSuchMethodException e) {
                    throw e;
                }
            } else {
                String setterMethodName = SETTER_PREFIX + StringUtils.capitalize(names[i]);
                invokeMethodByName(object, setterMethodName, new Object[]{value});
            }
        }
    }

    /**
     * 直接读取对象属性值, 无视private/protected修饰符, 不经过getter函数.
     *
     * @param obj       对象
     * @param fieldName 字段名
     * @throws IllegalAccessException 异常
     */
    @SuppressWarnings("unchecked")
    public static <E> E getFieldValue(final Object obj, final String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Field field = getField(obj.getClass(), fieldName);
        if (field == null) {
            logger.debug("在 [" + obj.getClass() + "] 中，没有找到 [" + fieldName + "] 字段 ");
            return null;
        }
        E result = null;
        try {
            result = (E) field.get(obj);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    /**
     * 直接设置对象属性值, 无视private/protected修饰符, 不经过setter函数.
     */
    public static <E> void setFieldValue(final Object obj, final String fieldName, final E value) throws NoSuchFieldException {
        Field field = getField(obj.getClass(), fieldName);
        if (field == null) {
            // throw new IllegalArgumentException("在 [" + obj.getClass() + "] 中，没有找到 [" + fieldName + "] 字段 ");
            logger.debug("在 [" + obj.getClass() + "] 中，没有找到 [" + fieldName + "] 字段 ");
            return;
        }
        try {
            field.set(obj, value);
        } catch (IllegalAccessException e) {
            logger.error("不可能抛出的异常: {}", e.getMessage());
        }
    }

    /**
     * 直接调用对象方法, 无视private/protected修饰符.
     * 用于一次性调用的情况，否则应使用getAccessibleMethod()函数获得Method后反复调用.
     * 同时匹配方法名+参数类型，
     */
    @SuppressWarnings("unchecked")
    public static <E> E invokeMethod(final Object obj, final String methodName, final Class<?>[] parameterTypes,
                                     final Object[] args) throws NoSuchMethodException {
        if (obj == null || methodName == null) {
            return null;
        }
        Method method = getAccessibleMethod(obj, methodName, parameterTypes);
        if (method == null) {
            logger.debug("在 [" + obj.getClass() + "] 中，没有找到 [" + methodName + "] 方法 ");
            return null;
        }
        try {
            return (E) method.invoke(obj, args);
        } catch (Exception e) {
            String msg = "method: " + method + ", obj: " + obj + ", args: " + Arrays.toString(args) + "";
            throw convertReflectionExceptionToUnchecked(msg, e);
        }
    }

    /**
     * 直接调用对象方法, 无视private/protected修饰符，
     * 用于一次性调用的情况，否则应使用getAccessibleMethodByName()函数获得Method后反复调用.
     * 只匹配函数名，如果有多个同名函数调用第一个。
     *
     * @param obj        对象
     * @param methodName 方法名
     * @param args       方法参数
     * @return 方法执行结果
     */
    @SuppressWarnings("unchecked")
    public static <E> E invokeMethodByName(final Object obj, final String methodName, final Object[] args) {
        Method method = getAccessibleMethodByName(obj, methodName, args.length);
        if (method == null) {
            // 如果为空不报错，直接返回空。
            logger.info("在 [" + obj.getClass() + "] 中，没有找到 [" + methodName + "] 方法 ");
            return null;
        }
        try {
            // 类型转换（将参数数据类型转换为目标方法参数类型）
//            Class<?>[] cs = method.getParameterTypes();
//            for (int i = 0; i < cs.length; i++) {
//                if (args[i] != null && !args[i].getClass().equals(cs[i])) {
//                    if (cs[i] == String.class) {
//                        args[i] = Convert.toStr(args[i]);
//                        if (StringUtils.endsWith((String) args[i], ".0")) {
//                            args[i] = StringUtils.substringBefore((String) args[i], ".0");
//                        }
//                    }
//                    else if (cs[i] == Integer.class) {
//                        args[i] = Convert.toInt(args[i]);
//                    }
//                    else if (cs[i] == Long.class) {
//                        args[i] = Convert.toLong(args[i]);
//                    }
//                    else if (cs[i] == Double.class) {
//                        args[i] = Convert.toDouble(args[i]);
//                    }
//                    else if (cs[i] == Float.class) {
//                        args[i] = Convert.toFloat(args[i]);
//                    }
//                    else if (cs[i] == Date.class) {
//                        if (args[i] instanceof String) {
//                            args[i] = DateUtils.parseDate(args[i]);
//                        }
//                        else {
//                            args[i] = DateUtil.getJavaDate((Double) args[i]);
//                        }
//                    }
//                    else if (cs[i] == boolean.class || cs[i] == Boolean.class) {
//                        args[i] = Convert.toBool(args[i]);
//                    }
//                }
//            }
            return (E) method.invoke(obj, args);
        } catch (Exception e) {
            String msg = "method: " + method + ", obj: " + obj + ", args: " + Arrays.toString(args) + "";
            throw convertReflectionExceptionToUnchecked(msg, e);
        }
    }

    /**
     * 循环向上转型, 获取对象的DeclaredMethod,并强制设置为可访问.
     * 如向上转型到Object仍无法找到, 返回null.
     * 匹配函数名+参数类型。
     * 用于方法需要被多次调用的情况. 先使用本函数先取得Method,然后调用Method.invoke(Object obj, Object... args)
     */
    public static Method getAccessibleMethod(final Object obj, final String methodName,
                                             final Class<?>... parameterTypes) throws NoSuchMethodException {
        // 为空不报错。直接返回 null
        if (obj == null) {
            return null;
        }
        Validate.notBlank(methodName, "methodName can't be blank");
        for (Class<?> searchType = obj.getClass(); searchType != Object.class; searchType = searchType.getSuperclass()) {
            try {
                Method method = searchType.getDeclaredMethod(methodName, parameterTypes);
                makeAccessible(method);
                return method;
            } catch (Exception e) {
                throw e;
            }

        }
        return null;
    }

    /**
     * 循环向上转型, 获取对象的DeclaredMethod,并强制设置为可访问.
     * 如向上转型到Object仍无法找到, 返回null.
     * 只匹配函数名。
     * 用于方法需要被多次调用的情况. 先使用本函数先取得Method,然后调用Method.invoke(Object obj, Object... args)
     */
    public static Method getAccessibleMethodByName(final Object obj, final String methodName, int argsNum) {
        // 为空不报错。直接返回 null
        if (obj == null) {
            return null;
        }
        Validate.notBlank(methodName, "methodName can't be blank");
        for (Class<?> searchType = obj.getClass(); searchType != Object.class; searchType = searchType.getSuperclass()) {
            Method[] methods = searchType.getDeclaredMethods();
            for (Method method : methods) {
                if (method.getName().equals(methodName) && method.getParameterTypes().length == argsNum) {
                    makeAccessible(method);
                    return method;
                }
            }
        }
        return null;
    }

    /**
     * 改变private/protected的方法为public，尽量不调用实际改动的语句，避免JDK的SecurityManager抱怨。
     */
    public static void makeAccessible(Method method) {
        if ((!Modifier.isPublic(method.getModifiers()) || !Modifier.isPublic(method.getDeclaringClass().getModifiers()))
                && !method.isAccessible()) {
            method.setAccessible(true);
        }
    }

    /**
     * 改变private/protected的成员变量为public，尽量不调用实际改动的语句，避免JDK的SecurityManager抱怨。
     */
    public static void makeAccessible(Field field) {
        if ((!Modifier.isPublic(field.getModifiers()) || !Modifier.isPublic(field.getDeclaringClass().getModifiers())
                || Modifier.isFinal(field.getModifiers())) && !field.isAccessible()) {
            field.setAccessible(true);
        }
    }

    /**
     * 通过反射, 获得Class定义中声明的泛型参数的类型, 注意泛型必须定义在父类处
     * 如无法找到, 返回Object.class.
     *
     * @param clazz 类
     * @return Class
     */
    public static <T> Class<T> getClassGenricType(final Class<T> clazz) {
        return getClassGenricType(clazz, 0);
    }

    /**
     * 通过反射, 获得Class定义中声明的父类的泛型参数的类型.
     * 如无法找到, 返回Object.class.
     *
     * @param clazz 类
     * @param index 索引
     * @return Type
     */
    public static <T> Class<T> getClassGenricType(final Class<T> clazz, final int index) {
        Type genType = clazz.getGenericSuperclass();

        if (!(genType instanceof ParameterizedType)) {
            logger.debug(clazz.getSimpleName() + "'s superclass not ParameterizedType");
            return null;
        }

        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();

        if (index >= params.length || index < 0) {
            logger.debug("Index: " + index + ", Size of " + clazz.getSimpleName() + "'s Parameterized Type: "
                    + params.length);
            return null;
        }
        if (!(params[index] instanceof Class)) {
            logger.debug(clazz.getSimpleName() + " not set the actual class on superclass generic parameter");
            return null;
        }

        return (Class<T>) params[index];
    }

    public static Class<?> getUserClass(Object instance) {
        if (instance == null) {
            throw new RuntimeException("Instance must not be null");
        }
        Class<?> clazz = instance.getClass();
        if (clazz != null && clazz.getName().contains(CGLIB_CLASS_SEPARATOR)) {
            Class<?> superClass = clazz.getSuperclass();
            if (superClass != null && !Object.class.equals(superClass)) {
                return superClass;
            }
        }
        return clazz;

    }

    /**
     * 获取属性上的注解
     */
    public static <T extends Annotation> T getFieldAnnotation(Field field, Class<T> annotationClass) {
        field.setAccessible(true);
        return field.getAnnotation(annotationClass);
    }

    /**
     * 获取属性上的注解
     * 你可以通过遍历 Field[] 数组 得到 Field对象 , 该对象有以下几种方法供你使用
     * getModifiers();获取访问修饰符
     * getName() 获取属性名
     * Typpe().getSimpleName();获取数据类型的简单名称
     *
     * @return 返回的是存在执行注解的字段
     */
    public static <T extends Annotation> Iterator<Map.Entry<Field, T>> getFieldAnnotation(Object object,
                                                                                          Class<T> annotationClass) {
        return getFieldAnnotation(getFieldsWithSuper(object), annotationClass);
    }

    public static <T extends Annotation, D> Iterator<Map.Entry<Field, T>> getFieldAnnotation(Class<D> objClass,
                                                                                             Class<T> annotationClass) {
        return getFieldAnnotation(getFieldsWithSuper(objClass), annotationClass);
    }

    /**
     * 代码抽离
     */
    private static <T extends Annotation> Iterator<Map.Entry<Field, T>> getFieldAnnotation(List<Field> fields,
                                                                                           Class<T> annotationClass) {
        Map<Field, T> map = new HashMap<>();
        for (Field field : fields) {
            T annotation = field.getAnnotation(annotationClass);
            if (annotation != null) {
                map.put(field, annotation);
            }
        }
        return map.entrySet().iterator();
    }

    /**
     * 获取类上的注解
     */
    public static <T extends Annotation> T getClassAnnotation(Class<?> c, Class<T> annotationClass) {
        boolean hasAnnotation = c.isAnnotationPresent(annotationClass);
        if (hasAnnotation) {
            return (T) c.getAnnotation(annotationClass);
        }
        return null;
    }

    /**
     * 通过属性上的注解获取属性名称
     *
     * @param object          对象
     * @param annotationClass 注解类
     * @return 返回null 说明没有获取到 属性上有执行的注解
     */
    public static <T extends Annotation> String getFieldNameByAnnotation(Object object, Class<T> annotationClass) {
        List<Field> allField = ReflectUtils.getFieldsWithSuper(object);
        for (Field field : allField) {
            T annotation = field.getAnnotation(annotationClass);
            if (annotation != null) {
                return field.getName();
            }
        }
        return null;
    }

    /**
     * 通过注解获取属性字段
     *
     * @param dClass          对象
     * @param annotationClass 注解
     * @return Field 字段
     */
    public static <T extends Annotation, D> Field getFieldByAnnotation(Class<D> dClass, Class<T> annotationClass) {
        List<Field> allField = ReflectUtils.getFieldsWithSuper(dClass);
        for (Field field : allField) {
            T annotation = field.getAnnotation(annotationClass);
            if (annotation != null) {
                return field;
            }
        }
        return null;
    }

    /**
     * 将反射时的checked exception转换为unchecked exception.
     */
    public static RuntimeException convertReflectionExceptionToUnchecked(String msg, Exception e) {
        if (e instanceof IllegalAccessException || e instanceof IllegalArgumentException
                || e instanceof NoSuchMethodException) {
            return new IllegalArgumentException(msg, e);
        } else if (e instanceof InvocationTargetException) {
            return new RuntimeException(msg, ((InvocationTargetException) e).getTargetException());
        }
        return new RuntimeException(msg, e);
    }

    /**
     * 正则替换指定值<br>
     * 通过正则查找到字符串，然后把匹配到的字符串加入到replacementTemplate中，$1表示分组1的字符串
     *
     * <p>
     * 例如：原字符串是：中文1234，我想把1234换成(1234)，则可以：
     *
     * <pre>
     * ReUtil.replaceAll("中文1234", "(\\d+)", "($1)"))
     *
     * 结果：中文(1234)
     * </pre>
     *
     * @param content             文本
     * @param regex               正则
     * @param replacementTemplate 替换的文本模板，可以使用$1类似的变量提取正则匹配出的内容
     * @return 处理后的文本
     */
    public static String replaceAll(CharSequence content, String regex, String replacementTemplate) {
        final Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        return replaceAll(content, pattern, replacementTemplate);
    }

    /**
     * 正则替换指定值<br>
     * 通过正则查找到字符串，然后把匹配到的字符串加入到replacementTemplate中，$1表示分组1的字符串
     *
     * @param content             文本
     * @param pattern             {@link Pattern}
     * @param replacementTemplate 替换的文本模板，可以使用$1类似的变量提取正则匹配出的内容
     * @return 处理后的文本
     * @since 3.0.4
     */
    public static String replaceAll(CharSequence content, Pattern pattern, String replacementTemplate) {
        if (StringUtils.isEmpty(content)) {
            return StringUtils.EMPTY;
        }

        final Matcher matcher = pattern.matcher(content);
        boolean result = matcher.find();
        if (result) {
            final Set<String> varNums = findAll(GROUP_VAR, replacementTemplate, 1, new HashSet<>());
            final StringBuffer sb = new StringBuffer();
            do {
                String replacement = replacementTemplate;
                for (String var : varNums) {
                    int group = Integer.parseInt(var);
                    replacement = replacement.replace("$" + var, matcher.group(group));
                }
                matcher.appendReplacement(sb, escape(replacement));
                result = matcher.find();
            }
            while (result);
            matcher.appendTail(sb);
            return sb.toString();
        }
        return Convert.toStr(content);
    }

    /**
     * 取得内容中匹配的所有结果
     *
     * @param <T>        集合类型
     * @param pattern    编译后的正则模式
     * @param content    被查找的内容
     * @param group      正则的分组
     * @param collection 返回的集合类型
     * @return 结果集
     */
    public static <T extends Collection<String>> T findAll(Pattern pattern, CharSequence content, int group,
                                                           T collection) {
        if (null == pattern || null == content) {
            return null;
        }

        if (null == collection) {
            throw new NullPointerException("Null collection param provided!");
        }

        final Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            collection.add(matcher.group(group));
        }
        return collection;
    }

    /**
     * 转义字符，将正则的关键字转义
     *
     * @param c 字符
     * @return 转义后的文本
     */
    public static String escape(char c) {
        final StringBuilder builder = new StringBuilder();
        if (RE_KEYS.contains(c)) {
            builder.append('\\');
        }
        builder.append(c);
        return builder.toString();
    }

    /**
     * 转义字符串，将正则的关键字转义
     *
     * @param content 文本
     * @return 转义后的文本
     */
    public static String escape(CharSequence content) {
        if (StringUtils.isBlank(content)) {
            return StringUtils.EMPTY;
        }

        final StringBuilder builder = new StringBuilder();
        int len = content.length();
        char current;
        for (int i = 0; i < len; i++) {
            current = content.charAt(i);
            if (RE_KEYS.contains(current)) {
                builder.append('\\');
            }
            builder.append(current);
        }
        return builder.toString();
    }


}
