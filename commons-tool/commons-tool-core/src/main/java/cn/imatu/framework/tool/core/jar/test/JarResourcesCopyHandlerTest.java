package cn.imatu.framework.tool.core.jar.test;

import cn.imatu.framework.tool.core.jar.CopyResourcesInfo;
import cn.imatu.framework.tool.core.jar.JarResourcesCopyHandler;

/**
 * @author shenguangyang
 */
public class JarResourcesCopyHandlerTest {
    public static void main(String[] args) throws Exception {
        String targetDirPath = JarResourcesCopyHandler
                .create(CopyResourcesInfo.builder().resourcesInfo(new TestDataResourcesInfo()).build())
                .addCopyAfterCallback((jarResourcesFile) -> !jarResourcesFile.getClassPath().endsWith(".md"))
                .addExcludeFile("**/*.md")
                .exec().getTargetDirPath();
        System.out.println(targetDirPath);
    }
}
