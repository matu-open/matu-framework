package cn.imatu.framework.tool.core.thread;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson2.JSON;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author shenguangyang
 * @date 2022-09-22 21:45
 */
class AsyncTaskExecutorTest {

    public static class PreTask {
        public String m1(String in) {
            System.out.println(Thread.currentThread().getName() + "-PreTask.m1, in: " + in);
            return in;
        }

        public void m2(String m1Out) {
            System.out.println(Thread.currentThread().getName() + "-PreTask.m2, in: " + m1Out);
        }
    }

    @Getter
    @Setter
    @Builder
    public static class MyData01 {
        private String name;
    }

    @Getter
    @Setter
    @Builder
    public static class MyData02 {
        private String name;
    }

    @Getter
    @Setter
    @ToString
    public static class MyData {
        private MyData01 myData01;
        private MyData02 myData02;
    }

    public static class Task {
        public String m1(String in) {
            System.out.println(Thread.currentThread().getName() + "-Task.m1, in: " + in);
            return in;
        }

        public void m2(String m1Out) {
            try {
                TimeUnit.MILLISECONDS.sleep(RandomUtil.randomInt(100, 200));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(Thread.currentThread().getName() + "-Task.m2, in: " + m1Out);
        }

        public MyData01 m3(String in) {
            System.out.println(Thread.currentThread().getName() + ", in: " + in);
            return MyData01.builder().name(in).build();
        }

        public MyData02 m4(String in) {
            System.out.println(Thread.currentThread().getName() + ", in: " + in);
            return MyData02.builder().name(in).build();
        }
    }

    @Test
    public void test1() throws Exception {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(10);
        System.out.println("main thread name: " + Thread.currentThread().getName());
        PreTask preTask = new PreTask();
        Task task = new Task();
        AsyncTaskExecutor.init(executor)
                .addPreTask(UUID.fastUUID().toString(), preTask::m1, preTask::m2)
                .addPreTask(UUID.fastUUID().toString(), preTask::m1, preTask::m2)
                .addTask(UUID.fastUUID().toString(), task::m1, task::m2)
                .addTask(UUID.fastUUID().toString(), task::m1, task::m2)
                .addTask(UUID.fastUUID().toString(), task::m1, task::m2)
                .addTask(UUID.fastUUID().toString(), task::m1, task::m2)
                .execute();
        System.out.println("exe end");
    }

    @Test
    public void test2() throws Exception {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(10);
        System.out.println("main thread name: " + Thread.currentThread().getName());

        MyData myData = new MyData();
        Task task = new Task();
        AsyncTaskExecutor.init(executor)
                .addTask("task.m3", task::m3, myData::setMyData01)
                .addTask("task.m4", task::m4, myData::setMyData02)
                .execute();
        System.out.println(JSON.toJSONString(myData));
        System.out.println("exe end");
    }
}