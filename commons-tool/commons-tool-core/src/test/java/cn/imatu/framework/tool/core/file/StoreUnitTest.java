package cn.imatu.framework.tool.core.file;

import cn.imatu.framework.tool.core.StoreUnit;
import cn.hutool.core.lang.Console;
import org.junit.jupiter.api.Test;

/**
 * @author shenguangyang
 * @date 2022-09-16 17:00
 */
class StoreUnitTest {
    @Test
    public void trans() {
        long bit = 1024L * 1024L * 1024L;
        Console.log("{} = {}", StoreUnit.BIT, StoreUnit.BIT.toBit(bit));
        Console.log("{} = {}", StoreUnit.BYTE, StoreUnit.BIT.toByte(bit));
        Console.log("{} = {}", StoreUnit.KB, StoreUnit.BIT.toKB(bit));
        Console.log("{} = {}", StoreUnit.MB, StoreUnit.BIT.toMB(bit));
        Console.log("{} = {}", StoreUnit.GB, StoreUnit.BIT.toGB(bit));
        Console.log("{} = {}", StoreUnit.TB, StoreUnit.BIT.toTB(bit));
        Console.log("===========================");
        Console.log("{} = {}", StoreUnit.BIT, StoreUnit.BIT.convert(bit, StoreUnit.BIT));
        Console.log("{} = {}", StoreUnit.BYTE, StoreUnit.BYTE.convert(bit, StoreUnit.BIT));
        Console.log("{} = {}", StoreUnit.KB, StoreUnit.KB.convert(bit, StoreUnit.BIT));
        Console.log("{} = {}", StoreUnit.MB, StoreUnit.MB.convert(bit, StoreUnit.BIT));
        Console.log("{} = {}", StoreUnit.GB, StoreUnit.GB.convert(bit, StoreUnit.BIT));
        Console.log("{} = {}", StoreUnit.TB, StoreUnit.TB.convert(bit, StoreUnit.BIT));
        Console.log("===========================");
        Console.log("autoUpper = {}", StoreUnit.autoUpper(bit, StoreUnit.BIT));
        Console.log("===========================");
        long byteSize = 1024L * 1024L * 1024L;
        Console.log("{} = {}", StoreUnit.MB, StoreUnit.MB.convert(byteSize, StoreUnit.BYTE));
        Console.log("{} = {}", StoreUnit.GB, StoreUnit.GB.convert(byteSize, StoreUnit.BYTE));
    }
}