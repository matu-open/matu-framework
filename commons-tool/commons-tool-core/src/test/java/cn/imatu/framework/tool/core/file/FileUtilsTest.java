package cn.imatu.framework.tool.core.file;

import cn.imatu.framework.tool.core.RegexUtils;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.UUID;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * @author shenguangyang
 */
class FileUtilsTest {

    @Test
    public void createBigFile() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 20; i++) {
            sb.append(UUID.randomUUID()).append("\n");
        }
        String data = sb.toString();
        for (int i = 0; i < 100000; i++) {
            FileUtil.appendString(data, "D:/a.txt", StandardCharsets.UTF_8);
        }

    }

    @Test
    void readLastLine() throws Exception {
        File file = new File("D:/a.txt"); // 100M
        long start = System.currentTimeMillis();
        String lastLine = FileUtils.readLastLine(file, "gbk");
        long time = System.currentTimeMillis() - start;
        System.out.println(lastLine);
        System.out.println("读取时间(毫秒):" + time);
    }

    @Test
    void checkExclude() {
        String test = RegexUtils.getRegex("****/test/**");
        System.out.println(test);
        boolean b = FileUtils.checkExclude("BOOT_INF/test/test/01.md", new ArrayList<String>() {{
            add(test);
        }});
        System.out.println(b);
    }
}