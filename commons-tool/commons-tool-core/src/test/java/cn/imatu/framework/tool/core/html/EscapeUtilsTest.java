package cn.imatu.framework.tool.core.html;

import org.junit.jupiter.api.Test;

/**
 * @author shenguangyang
 * @date 2022-10-06 10:24
 */
class EscapeUtilsTest {

    @Test
    public void test() {
//        String html = "<script>alert(1);</script>";
        String html = "<scr<script>ipt>alert(\"XSS\")</scr<script>ipt>";
        // String html = "<123";
        // String html = "123>";
        String escape = EscapeUtils.escape(html);
        System.out.println("clean: " + EscapeUtils.clean(html));
        System.out.println("escape: " + escape);
        System.out.println("unescape: " + EscapeUtils.unescape(escape));
    }
}