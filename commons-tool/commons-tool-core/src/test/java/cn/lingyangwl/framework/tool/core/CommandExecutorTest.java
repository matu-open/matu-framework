package cn.imatu.framework.tool.core;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Map;

/**
 * @author shenguangyang
 */
@Slf4j
class CommandExecutorTest {

    public static void main(String[] args) throws Exception {
//        String[] cmd = {"/bin/bash", "-c", "sh build.sh && cd ../cpp-base && sh build.sh"};
//        String[] cmd = {"cmd.exe", "/c", "ping www.baidu.com"};
//        String[] cmd = {"cmd.exe", "/c", "java -version"};
        String[] cmd = {"cmd.exe", "/c", "findstr /N /O 10 d2c3594deb77e61a info.log"};
        CommandExecutor commandExecutor = new CommandExecutor();
        Map<String, String> environmentVariables = System.getenv();
        // commandExecutor.executeCommand(command, new File("/home"), environmentVariables);
        CommandExecutor.Result result = commandExecutor.executeCommandAndReturnResult(cmd, new File("/home"), environmentVariables);
        if (result.isSuccess()) {
            log.info("exec success: \n{}", result.getSuccessResult());
        } else {
            log.error("exec fail: \n{}", result.getErrorResult());
        }

        if (result.hasWarn()){
            log.warn("exec warn: \n{}", result.getWarnResult());
        }
    }

}