package cn.imatu.framework.tool.core;

import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.util.RandomUtil;
import cn.imatu.framework.tool.core.tree.TreeNode;
import cn.imatu.framework.tool.core.tree.TreeUtils;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author shenguangyang
 */
class TreeUtilsTest {

    @Data
    public static class TestData implements TreeNode<Long, TestData> {
        private Long id;
        private Long parentId;
        private Integer sortNo;
        private String name;
        private List<TestData> children = new ArrayList<>();
        private String treeNames;
        private String treeLeaf;
        private Integer treeLevel;
        private String treeParentIds;

        @Override
        public Long treeId() {
            return id;
        }

        @Override
        public void treeId(Long id) {
            this.id = id;
        }

        @Override
        public Long treeParentId() {
            return parentId;
        }

        @Override
        public void treeParentId(Long parentId) {
            this.parentId = parentId;
        }

        @Override
        public List<TestData> children() {
            return this.children;
        }

        @Override
        public String name() {
            return this.name;
        }

        @Override
        public void children(List<TestData> children) {
            this.children = children;
        }

        @Override
        public Integer sortNo() {
            return this.sortNo;
        }

        @Override
        public void treeLeaf(Boolean treeLeaf) {
            this.treeLeaf = treeLeaf ? "YES" : "NO";
        }

        @Override
        public void treeLevel(Integer treeLevel) {
            this.treeLevel = treeLevel;
        }

        @Override
        public void treeNames(String names) {
            this.treeNames = names;
        }

        @Override
        public void treeParentIds(String parentIds) {
            this.treeParentIds = parentIds;
        }
    }

    public static List<TestData> createTree() {

        TestData t = new TestData();
        t.setId(0L);
        t.setName("root");

        TestData t01 = new TestData();
        t01.setId(1L);
        t01.setParentId(0L);
        t01.setName("t01");

        TestData t02 = new TestData();
        t02.setId(2L);
        t02.setParentId(0L);
        t02.setName("t02");

        TestData t04_1 = new TestData();
        t04_1.setId(3L);
        t04_1.setParentId(6L);
        t04_1.setName("t04_1");

        TestData t04_1_1 = new TestData();
        t04_1_1.setId(4L);
        t04_1_1.setParentId(3L);
        t04_1_1.setName("t04_1_1");

        TestData t04_1_2 = new TestData();
        t04_1_2.setId(5L);
        t04_1_2.setParentId(3L);
        t04_1_2.setName("t04_1_2");

        TestData t04_1_2_1 = new TestData();
        t04_1_2_1.setId(20L);
        t04_1_2_1.setParentId(5L);
        t04_1_2_1.setName("t04_1_2_1");
        t04_1_2.setChildren(Collections.singletonList(t04_1_2_1));

        t04_1.setChildren(Arrays.asList(t04_1_1, t04_1_2));

        TestData t04 = new TestData();
        t04.setId(6L);
        t04.setChildren(Collections.singletonList(t04_1));
        t04.setParentId(0L);
        t04.setName("t04");

        List<TestData> list = new ArrayList<>();
        list.add(t01);
        list.add(t02);
        list.add(t04);

        t.setChildren(list);
        List<TestData> resp = new ArrayList<>();
        resp.add(t);
        return resp;
    }

    public static List<TestData> createList() {
        List<TestData> resp = new ArrayList<>();

        TestData t01 = new TestData();
        t01.setId(1L);
        t01.setParentId(0L);
        t01.setName("t01");
        resp.add(t01);

        TestData t02 = new TestData();
        t02.setId(2L);
        t02.setParentId(0L);
        t02.setName("t02");
        resp.add(t02);

        TestData t04_1 = new TestData();
        t04_1.setId(3L);
        t04_1.setParentId(6L);
        t04_1.setName("t04_1");
        resp.add(t04_1);

        TestData t04_1_1 = new TestData();
        t04_1_1.setId(4L);
        t04_1_1.setParentId(3L);
        t04_1_1.setName("t04_1_1");
        resp.add(t04_1_1);

        TestData t04_1_2 = new TestData();
        t04_1_2.setId(5L);
        t04_1_2.setParentId(3L);
        t04_1_2.setName("t04_1_2");
        resp.add(t04_1_2);

        TestData t04_1_2_1 = new TestData();
        t04_1_2_1.setId(20L);
        t04_1_2_1.setParentId(5L);
        t04_1_2_1.setName("t04_1_2_1");
        resp.add(t04_1_2_1);

        TestData t04 = new TestData();
        t04.setId(6L);
        t04.setParentId(0L);
        t04.setName("t04");
        resp.add(t04);

        return resp;
    }

    @Test
    public void listChildrenId() {
        List<TestData> list = createList();
        List<TestData> tree = TreeUtils.build(list, 0L);
        Set<Long> build = TreeUtils.listChildrenId(list, 6L);
        System.out.println(JSON.toJSONString(build, JSONWriter.Feature.PrettyFormat));
    }

    @Test
    public void buildTree() {
        List<TestData> list = createList();
        List<TestData> build = TreeUtils.build(list, 0L);
        System.out.println(JSON.toJSONString(build, JSONWriter.Feature.PrettyFormat));
    }

    @Test
    public void buildTreeAndCallback() {
        List<TestData> list = createList();
        List<TestData> build = TreeUtils.build(list, 0L, node -> System.out.println(JSON.toJSONString(node)));
        System.out.println(JSON.toJSONString(build, JSONWriter.Feature.PrettyFormat));
    }

    @Test
    public void listFlatChild() {
        List<TestData> list = createList();
        List<TestData> tree = TreeUtils.build(list, 0L);
        TreeUtils.listFlatChild(createList(), Collections.singletonList(3L), (node) -> {
            System.out.println(JSON.toJSONString(node));
        });
    }

    @Test
    public void listFlatParent() {
        List<TestData> list = createList();
        List<TestData> tree = TreeUtils.build(list, 0L);
        List<TestData> testData = TreeUtils.listFlatParent(createList(), Collections.singletonList(3L), (node) -> {
            System.out.println(JSON.toJSONString(node));
        });
    }

    @Test
    public void filterTree() {
        List<TestData> list = createList();
        List<TestData> treeList = TreeUtils.build(list, 0L);
        List<TestData> filterList = TreeUtils.filter(treeList, e -> e.getName().contains("t04_1"));
        System.out.println(JSON.toJSONString(filterList, JSONWriter.Feature.PrettyFormat));
    }

    @Test
    public void filterNewTree() {
        List<TestData> list = createList();
        List<TestData> treeList = TreeUtils.build(list, 0L);
        List<TestData> filterList = TreeUtils.filterNew(treeList, e -> e.getName().contains("t04_1"));
        System.out.println(JSON.toJSONString(filterList, JSONWriter.Feature.PrettyFormat));
    }

    @Test
    void foreachTree() {
        TreeUtils.foreachTree(createTree(), (parentNode, currentNode) -> {
            System.out.println(currentNode.getName());
        });
    }

    @Test
    void foreachFlat() {
        TreeUtils.foreachFlat(0L, createList(), (parentNode, currentNode) -> {
            System.out.println(currentNode.getName());
        });
    }

    @Test
    public void copyTree() {
        TestData testData = TreeUtils.copyTree(createTree(), 0L, 3L, (node) -> {
            if (node.getId() == 0L) {
                return node.getId();
            }
            return RandomUtil.randomLong(0, 10000);
        });
        List<TestData> list = TreeUtils.flatTree(testData);
        System.out.println(testData);
    }

    @Getter
    @Setter
    public static class Area implements TreeNode<String, Area> {
        protected String areaId;

        protected String areaName;

        protected String parentId;

        /**
         * 所有父级编号
         */
        protected String parentIds;

        /**
         * 是否为叶子节点(最末级)
         */
        protected String treeLeaf;

        /**
         * 层次级别
         */
        protected Integer treeLevel;

        /**
         * 全节点名
         */
        protected String treeNames;

        private List<Area> children;

        @Override
        public String treeId() {
            return this.areaId;
        }

        @Override
        public void treeId(String treeId) {
            this.areaId = treeId;
        }

        @Override
        public String treeParentId() {
            return this.parentId;
        }

        @Override
        public void treeParentId(String parentId) {
            this.parentId = parentId;
        }

        @Override
        public List<Area> children() {
            return this.children;
        }

        @Override
        public String name() {
            return this.getAreaName();
        }

        @Override
        public void children(List<Area> children) {
            this.children = children;
        }

        @Override
        public Integer sortNo() {
            return 0;
        }
    }

    /**
     * 测试速度
     */
    @Test
    public void testSpeed() {
        for (int i = 0; i < 10; i++) {
            String data = ResourceUtil.readUtf8Str("tree_test_area.json");
            List<Area> areas = JSON.parseArray(data, Area.class);
            StopWatch started = StopWatch.createStarted();
            List<Area> build = TreeUtils.build(areas, "0");
            System.out.println("time: " + started.getTime(TimeUnit.MILLISECONDS) + " ms");
        }
    }
}