package cn.imatu.framework.tool.core;

import cn.imatu.framework.exception.BaseError;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @author shenguangyang
 */
@Getter
@AllArgsConstructor
public enum TestErrorEnum implements BaseError {
    ERROR(-1, "200");
    private final Integer code;
    private final String message;

    @Override
    public HttpStatus getStatus() {
        return null;
    }
}
