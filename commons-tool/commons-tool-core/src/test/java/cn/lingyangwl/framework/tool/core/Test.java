package cn.imatu.framework.tool.core;

import cn.hutool.core.util.RandomUtil;
import cn.imatu.framework.tool.core.exception.Assert;

import java.io.File;

/**
 * @author shenguangyang
 * @date 2022-08-17 18:48
 */
public class Test {
    public static void main(String[] args) {
        File file = new File("/test/pom.xml");
        System.out.println();
        System.out.println(file.getAbsoluteFile().getPath());
        System.out.println(file.getPath());
        String tes  = RandomUtil.randomInt(1, 5) > 1 ? null : "";
        Assert.notNull(tes, TestErrorEnum.ERROR);

    }
}
