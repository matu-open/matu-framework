package cn.imatu.framework.tool.core;

import org.junit.jupiter.api.Test;

/**
 * @author shenguangyang
 * @date 2022-10-28 9:06
 */
class InputCoderTest {
    @Test
    public void t01() {
        System.out.println(InputCoder.getPinYin("你好啊! 中国"));
        System.out.println(InputCoder.getFirstSpell("你好啊! 中国"));
        System.out.println(InputCoder.getFullSpell("你好啊! 中国"));
    }
}