package cn.imatu.framework.tool.core;

import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 * @author shenguangyang
 * @date 2022-08-17 11:05
 */
class MD5UtilsTest {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        String plainText = "admin";
        String saltValue = "admin123";

        System.out.println(MD5Utils.md5Lower(plainText));
        System.out.println(MD5Utils.md5Upper(plainText));
        System.out.println(MD5Utils.md5Lower(plainText, saltValue));
        System.out.println(MD5Utils.md5Upper(plainText, saltValue));
        System.out.println(MD5Utils.md5(plainText));
        System.out.println("=====校验结果======");
        System.out.println(MD5Utils.valid(plainText, Objects.requireNonNull(MD5Utils.md5(plainText))));
    }
}