package cn.imatu.framework.tool.core;

import cn.imatu.framework.tool.core.file.JarUtils;
import cn.imatu.framework.tool.core.jar.test.TestDataResourcesInfo;
import org.junit.jupiter.api.Test;

/**
 * @author shenguangyang
 */
class JarUtilsTest {

    @Test
    void copyDir() {
        JarUtils.copyDataFromClasses("/temp/jar-file-data", TestDataResourcesInfo.class);
    }
}