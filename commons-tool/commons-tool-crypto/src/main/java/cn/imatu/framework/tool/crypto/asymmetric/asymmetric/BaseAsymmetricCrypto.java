package cn.imatu.framework.tool.crypto.asymmetric;

import cn.hutool.crypto.asymmetric.KeyType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public abstract class BaseAsymmetricCrypto {

    public abstract String decrypt(String data, KeyType keyType, String key);
    public abstract String encrypt(String data, KeyType keyType, String privateKey);

    /**
     * 获取公钥和私钥
     */
    public abstract CryptoKeyDto getKey();

    public abstract AsymmetricCryptoType type();
}
