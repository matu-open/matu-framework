package cn.imatu.framework.tool.crypto.asymmetric;

import java.util.Arrays;

/**
 * 非对称加密类型
 *
 * @author shenguangyang
 */
public enum AsymmetricCryptoType {
    RSA, SM2;
    public static AsymmetricCryptoType of(String name) {
        return Arrays.stream(values())
                .filter(e -> e.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(null);
    }
}
