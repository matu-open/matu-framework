package cn.imatu.framework.tool.crypto.asymmetric;

/**
 * @author shenguangyang
 */
public enum RsaKeyType {
    PUBLIC, PRIVATE
}
