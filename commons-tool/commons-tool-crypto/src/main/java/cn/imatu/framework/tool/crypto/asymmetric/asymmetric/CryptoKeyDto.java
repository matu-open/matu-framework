package cn.imatu.framework.tool.crypto.asymmetric;

import lombok.*;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CryptoKeyDto {
    private String privateKey;
    private String publicKey;
}
