package cn.imatu.framework.tool.crypto;

import cn.hutool.core.util.RandomUtil;
import cn.imatu.framework.tool.crypto.asymmetric.AesCrypto;
import cn.imatu.framework.tool.crypto.asymmetric.CryptoException;

/**
 * @author shenguangyang
 */
class AesUtilsTest {

    public static void main(String[] args) throws CryptoException {
        String key = RandomUtil.randomString(32);
        AesCrypto aesCrypto = new AesCrypto(key);
        System.out.println(key);
        String encryptData = aesCrypto.encrypt("test123");
        System.out.println("加密：" + encryptData);
        String decryptData = aesCrypto.decrypt(encryptData);
//        String decryptData1 = new AesCrypto(RandomUtil.randomString(32)).decrypt(encryptData);
        System.out.println("解密：" + decryptData);
//        System.out.println("解密1：" + decryptData1);
    }
}