package cn.imatu.framework.tool.crypto.asymmetric;

import cn.hutool.crypto.asymmetric.KeyType;

/**
 * @author shenguangyang
 */
class BaseAsymmetricCryptoTest {
    public static void main(String[] args) {
        BaseAsymmetricCrypto asymmetricCrypto = AsymmetricCryptoManager.getInstance("rsa");
        CryptoKeyDto cryptoKeyDto = asymmetricCrypto.getKey();
        String encrypt = asymmetricCrypto.encrypt("123", KeyType.PublicKey, cryptoKeyDto.getPublicKey());
        System.out.println("encrypt: " + encrypt);
        String decrypt = asymmetricCrypto.decrypt(encrypt, KeyType.PrivateKey, cryptoKeyDto.getPrivateKey());
        System.out.println("decrypt: " + decrypt);
    }

}