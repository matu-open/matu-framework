package cn.imatu.framework.sign.core;

import cn.imatu.framework.tool.core.exception.Assert;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @author shenguangyang
 */
@ToString
@Accessors(chain = true)
public class SignPayload {
    /**
     * 时间戳
     */
    public static final String TIME_STAMP = "timeStamp";
    /**
     * 签名数据
     */
    public static final String SIGN = "sign";
    /**
     * 随机数，客户端生成随机数主要保证签名的多变性
     */
    public static final String NONCE = "nonce";
    /**
     * 密钥id
     */
    public static final String SECRET_ID = "secretId";
    public static final String SECRET_KEY = "secretKey";
    /**
     * 签名类型, 比如md5/sha256
     */
    public static final String SIGN_TYPE = "signType";

    /**
     * 时间戳
     */
    @Getter
    @Setter
    private String timeStamp;

    /**
     * 签名
     */
    @Getter
    @Setter
    private String sign;

    /**
     * 随机数，生成签名方生成随机数主要保证签名的多变性
     */
    @Getter
    @Setter
    private String nonce;

    /**
     * 密钥id
     */
    @Getter
    @Setter
    private String secretId;

    /**
     * 签名类型
     */
    @Getter
    @Setter
    private String signType;

    public static void check(JSONObject data) {
        SignPayload signPayload = JSON.to(SignPayload.class, data);
        signPayload.check();
    }

    public void check() {
        // 判断需要的参数是否为空，不为空进行后续操作
        Assert.isFalse(StringUtils.isEmpty(nonce) || "null".equalsIgnoreCase(nonce), NONCE + "不能为空");
        Assert.isFalse(StringUtils.isEmpty(sign) || "null".equalsIgnoreCase(sign), SIGN + "不能为空");
        Assert.isFalse(StringUtils.isEmpty(secretId) || "null".equalsIgnoreCase(secretId), SECRET_ID + "不能为空");
        Assert.isFalse(StringUtils.isEmpty(timeStamp) || "null".equalsIgnoreCase(timeStamp), TIME_STAMP + "不能为空");
        Assert.isFalse(StringUtils.isEmpty(signType) || "null".equalsIgnoreCase(signType), SIGN_TYPE + "不能为空");
    }

    public SignPayload() {

    }

    public static String[] getAllPayload() {
        return new String[] { SIGN, SECRET_ID, SECRET_KEY, NONCE, TIME_STAMP, SIGN_TYPE};
    }

    /**
     * 转成map
     */
    public Map<String, Object> toMapOfObjectType() {
        Map<String, Object> map = new HashMap<>();
        map.put(SECRET_ID, this.secretId);
        map.put(NONCE, this.nonce);
        map.put(SIGN, this.sign);
        map.put(TIME_STAMP, this.timeStamp);
        map.put(SIGN_TYPE, this.signType);
        return map;
    }

    public Map<String, String> toMapOfStringType() {
        Map<String, String> map = new HashMap<>();
        map.put(NONCE, this.nonce);
        map.put(SECRET_ID, this.secretId);
        map.put(SIGN, this.sign);
        map.put(TIME_STAMP, this.timeStamp);
        map.put(SIGN_TYPE, this.signType);
        return map;
    }


    public Map<String, List<String>> toMultiValueMapForStringType() {
        Map<String, List<String>> map = new HashMap<>();
        map.put(SECRET_ID, Collections.singletonList(this.secretId));
        map.put(NONCE, Collections.singletonList(this.nonce));
        map.put(TIME_STAMP, Collections.singletonList(this.timeStamp));
        map.put(SIGN_TYPE, Collections.singletonList(this.signType));
        map.put(SIGN, Collections.singletonList(this.sign));
        return map;
    }

    public static SignPayload buildByMapOfStringType(Map<String, String> map) {
        return new SignPayload()
                .setNonce(map.get(NONCE)).setSignType(map.get(SIGN_TYPE)).setTimeStamp(map.get(TIME_STAMP))
                .setSign(map.get(SIGN)).setSecretId(map.get(SECRET_ID));
    }

    public static SignPayload buildByFunction(Function<String, String> function) {
        Map<String, String> map = new HashMap<>();
        for (String key : getAllPayload()) {
            map.put(key, function.apply(key));
        }
        return new SignPayload()
            .setNonce(map.get(NONCE)).setSignType(map.get(SIGN_TYPE)).setTimeStamp(map.get(TIME_STAMP))
            .setSign(map.get(SIGN)).setSecretId(map.get(SECRET_ID));
    }

    public static SignPayload buildByMapOfObjectType(Map<String, Object> map) {
        return new SignPayload()
                .setNonce(String.valueOf(map.get(NONCE))).setSignType(String.valueOf(map.get(SIGN_TYPE)))
                .setTimeStamp(String.valueOf(map.get(TIME_STAMP)))
                .setSign(String.valueOf(map.get(SIGN))).setSecretId(String.valueOf(map.get(SECRET_ID)));
    }

    public static SignPayload buildByMultiValueMapOfStringType(Map<String, List<String>> map) {
        return new SignPayload()
                .setNonce(getFirst(map, NONCE)).setSignType(getFirst(map, SIGN_TYPE)).setTimeStamp(getFirst(map, TIME_STAMP))
                .setSign(getFirst(map, SIGN)).setSecretId(getFirst(map, SECRET_ID));
    }

    private static String getFirst(Map<String, List<String>> map, String key) {
        return map.getOrDefault(key, Collections.emptyList()).stream().findFirst().orElse(null);
    }
}
