package cn.imatu.framework.sign.core;

import cn.hutool.crypto.digest.DigestUtil;

/**
 * @author shenguangyang
 */
public class Sha256AlgoProcess extends HashAlgoProcess {
    @Override
    public String process(String req) throws Exception {
        return DigestUtil.sha256Hex(req);
    }

    @Override
    public SignType getSignTYpe() {
        return SignType.SHA256;
    }
}
