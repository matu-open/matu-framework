package cn.imatu.framework.sign.core;

/**
 * 签名算法
 * @author shenguangyang
 */
public interface SignAlgoProcess {
    SignType getSignTYpe();
}
