package cn.imatu.framework.sign.core;

/**
 * @author shenguangyang
 */
public enum RsaKeyType {
    PUBLIC, PRIVATE
}
