# 通用的公共库简介

封装了很多常用的库, 方便用户集成到自己项目中并进行快速开发

# 相关文档

https://www.yuque.com/shenguangyang/rhoz1v

# 涵盖的公共库
 
| 模块名                                   | 说明                                                                           |
| ------------------------------------- | ---------------------------------------------------------------------------- |
| cache-spring-boot-starter             | 对redis缓存中间件的二次封装                                                             |
| core-spring-boot-starter              | 核心公共组件, 包含Resp响应体接口(可适配自己项目的响应格式)                                            |
| data-mate-spring-boot-starter         | 数据助手组件用于对数据的处理, 比如字典回写, 数据脱敏等功能                                              |
| database-spring-boot-starter          | 对数据库的处理, eg: 针对mysql初始化时候执行用户配置的脚本以便创建数据库等操作(数据库不用手动去创建, 项目启动会自动创建)          |
| orm-spring-boot-starter               | 对mybatis plus等常用orm框架进行二次封装                                                  |
| idgenerator-spring-boot-starter       | 雪花算法id生成器, 对yitter-idgenerator框架进行封装                                         |
| grpc-spring-boot-starter              | 只grpc引入相关依赖                                                                  |
| ip2region-spring-boot-starter         | ip转地理位置组件, 支持离线                                                              | |
| knife4j-spring-boot-starter      | 用于微服务项目中各个服务中在线文档聚合, 可以用于gateway以及web服务, 然后gateway中可以收集所有微服务的接口文档            |
| lock-spring-boot-starter              | 分布式锁封装, 支持redis, zookeeper分布式锁                                               |
| log-spring-boot-starter               | 对日志的封装, 支持微服务链路跟踪, 日志中会打印traceId(一个请求中traceId是唯一的)                           |
| mq-spring-boot-starter                | 对常用消息队列框架封装, 目前已封装mqtt、kafka、rabbitmq、rocketmq等消息队列                          |
| security-spring-boot-starter          | 安全组件, 支持xss攻击、接口限流、接口加签名                                                     |
| sms-spring-boot-starter               | 短信组件, 支持阿里云短信平台、个人测试用的短信平台(聚美JM)                                             |
| storage-spring-boot-starter           | 存储服务, 统一对外暴露接口, 支持minio, oss存储服务                                             |
| mock-spring-boot-starter              | 用于mock数据底层调用了mock.js                                                         |
| dynamic-datasource-spring-boot-starter | 封装dynamic-datasource-spring-boot-starter                                     |
| web-spring-boot-starter               | 对springboot-web封装, 支持全局统一异常处理等功能                                             |
| commons-tool-core                     | 包含常用工具类、异常类, eg: BaseException, BaseError(枚举),多线程任务执行器AsyncTaskExecutor等等工具类 |
| commons-tool-crypto                   | 依赖hutool-crypto, 并添加自己用到的加密解密工具类                                             |
| commons-tool-captcha                  | 验证码工具类, 依赖hutool-captcha, 并添加自己的工具类                                          |
