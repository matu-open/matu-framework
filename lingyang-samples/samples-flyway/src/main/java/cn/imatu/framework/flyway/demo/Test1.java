package cn.imatu.framework.flyway.demo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author shenguangyang
 */
@Data
@TableName("test1")
public class Test1 {
    private Long accessKeyId;
    private Long userId;
    private String secretAccessKey;
}
