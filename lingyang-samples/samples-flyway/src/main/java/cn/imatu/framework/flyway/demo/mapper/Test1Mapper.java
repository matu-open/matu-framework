package cn.imatu.framework.flyway.demo.mapper;

import cn.imatu.framework.flyway.demo.Test1;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author shenguangyang
 */
public interface Test1Mapper extends BaseMapper<Test1> {
}
