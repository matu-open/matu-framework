-- ----------------------------------------------
-- 仅需要被执行一次的SQL命名以大写的"V"开头，后面跟上"0~9"数字的组合,数字之间可以用“.”或者下划线"_"分割开，
-- 然后再以两个下划线分割，其后跟文件名称，最后以.sql结尾。
-- 比如，V2.1.5__create_user_ddl.sql、V4.1_2__add_user_dml.sql
-- ----------------------------------------------


/*
 Navicat Premium Data Transfer

 Source Server         : wsl2
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : ubuntu2004.wsl:53306
 Source Schema         : ums

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 19/06/2022 16:32:44
*/

-- ----------------------------
-- Table structure for sys_access_key
-- ----------------------------
CREATE TABLE IF NOT EXISTS `test1`  (
  `access_key_id` bigint(0) NOT NULL COMMENT '用于标识访问者的身份',
  `user_id` bigint(0) NOT NULL,
  `secret_access_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用于加密签名字符串和服务器端验证签名字符串的密钥，必须严格保密。',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '状态,1正常 , -1禁用',
  `create_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`access_key_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

