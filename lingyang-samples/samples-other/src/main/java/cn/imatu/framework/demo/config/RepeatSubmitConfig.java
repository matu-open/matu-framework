package cn.imatu.framework.demo.config;

import cn.imatu.framework.core.utils.servlet.ServletUtils;
import cn.imatu.framework.security.submit.RepeatSubmitDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author shenguangyang
 */
@Configuration
public class RepeatSubmitConfig {
    @Bean
    public RepeatSubmitDefinition repeatSubmitDefinition() {
        return RepeatSubmitDefinition.builder()
            .tokenSupplier(() -> {
                return ServletUtils.getRequest().get().getHeader("x-login-id");
            }).build();
    }
}
