package cn.imatu.framework.demo;

import cn.imatu.framework.knife4j.micro.servlet.ApiGroupConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shenguangyang
 */
@ApiGroupConfig(groupName = "test")
@SpringBootApplication(scanBasePackages = {
        "cn.imatu.framework.demo"
})
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
