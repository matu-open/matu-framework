package cn.imatu.framework.samples;

import cn.imatu.framework.log.*;
import cn.imatu.framework.log.event.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author shenguangyang 异步监听日志事件
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class TestWebLogListener {

	@Async
	@Order
	@EventListener(WebLogEvent.class)
	public void saveSysLog(WebLogEvent event) {
		WebLogInfo info = (WebLogInfo) event.getSource();
		System.out.println("================" + info.getContent());
	}
}
