package cn.imatu.framework.samples;

import cn.imatu.framework.log.*;
import cn.imatu.framework.log.annotation.*;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author shenguangyang
 */
@Component
public class TestRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        OperateLogAnnotScan.allLogAnnotMap.forEach((k, v) -> {
            for (OperateLog operateLog : v) {
                System.out.println("eventObject: " + k.eventObject() + ", eventType: " + operateLog.eventType());
            }

        });
        System.out.println("test " + OperateLogAnnotScan.allLogAnnotMap.size());
    }
}
