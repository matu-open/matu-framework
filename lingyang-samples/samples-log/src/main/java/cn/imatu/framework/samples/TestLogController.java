package cn.imatu.framework.samples;

import cn.hutool.http.HttpUtil;
import cn.imatu.framework.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@Slf4j
@RestController
@RequestMapping("/testLog")
@EnableFeignClients
public class TestLogController {

//    @Resource
//    BaggageField countryCodeField;

    @Resource
    private TestFeign testFeign;
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private RabbitTemplate rabbitTemplate;

    @PostConstruct
    public void init() {

    }

    @GetMapping("/")
    public String test1() {
//        countryCodeField.updateValue("new-value");
        log.trace("Trace 日志...");
        log.debug("Debug 日志...");
        log.info("Info 日志...");
        log.warn("Warn 日志...");
        log.error("Error 日志...");
        log.error("Error 日志...", new BizException("test"));
        testFeign.test2("feign");
        log.info("=====> feign end");

        HttpUtil.get("http://127.0.0.1:38080/testLog/test2?name=hutool");
        log.info("=====> hutool end");

        restTemplate.getForEntity("http://127.0.0.1:38080/testLog/test2?name=restTemplate", String.class);
        log.info("=====> restTemplate end");

        rabbitTemplate.convertAndSend("test.log.exchange",  "", "from rabbitmq");
        log.info("=====> rabbitmq end");

//        rabbitTemplate.convertAndSend("test.log.exchange",  "", "from rabbitmq mock fail");
//        log.info("=====> rabbitmq mock fail");
        return "ok";
    }

    @GetMapping("/test2")
    public String test2(String name) {
        log.info("\n\n==============================> from {}", name);
//        countryCodeField.updateValue("new-value");
        log.trace("Trace 日志...");
        log.debug("Debug 日志...");
        log.info("Info 日志...");
        log.warn("Warn 日志...");
        log.error("Error 日志...");
        return "ok";
    }
}
