package cn.imatu.framework.samples;

import cn.imatu.framework.exception.BizException;
import cn.imatu.framework.tool.core.StringUtils;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @author shenguangyang
 */
@Slf4j
@Component
@RabbitListener(bindings = {
        @QueueBinding(
                value = @Queue(value = "test.log.queue", durable = "true"),
                exchange = @Exchange(value = "test.log.exchange", type = ExchangeTypes.TOPIC))
})
public class TestLogMqListener {

    @RabbitHandler
    public void onMessage(@Payload String msg, Channel channel, Message message) {
        try {
            log.info("\n\n==============================> from rabbitmq");
            if (StringUtils.contains(msg, "fail")) {
                throw new BizException("模拟消费失败");
            }
            log.info("test-log ===> 消费成功: {}", msg);
        } catch (Exception e) {
            log.error("test-log ===> 消费失败: ", e);
            throw new BizException(e.getMessage());
        }
    }
}
