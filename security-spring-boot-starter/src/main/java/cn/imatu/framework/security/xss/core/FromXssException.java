package cn.imatu.framework.security.xss.core;

import cn.imatu.framework.exception.BaseException;
import lombok.Getter;

/**
 * xss 表单异常
 *
 * @author shenguangyang
 */
@Getter
public class FromXssException extends BaseException {

    private final String input;

    public FromXssException(String input, String message) {
        super(message);
        this.input = input;
    }

}
