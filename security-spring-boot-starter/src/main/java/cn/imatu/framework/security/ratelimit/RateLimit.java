package cn.imatu.framework.security.ratelimit;

import java.lang.annotation.*;

/**
 * 限流注解
 *
 * @author shenguangyang
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RateLimit {
    /**
     * 限流key
     */
    String key() default "default";

    /**
     * 限流时间,单位秒
     */
    int time() default 60;

    /**
     * 限流次数
     */
    int count() default 100;

    /**
     * 限流类型
     */
    LimitTypeEnum type() default LimitTypeEnum.DEFAULT;

    /**
     * 得不到令牌的提示语 </br>
     * @see RateLimitCons
     */
    String msg() default "访问过于频繁, 剩余 ${remainingTime}";
}
