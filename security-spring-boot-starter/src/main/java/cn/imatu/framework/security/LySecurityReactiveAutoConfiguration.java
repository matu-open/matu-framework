package cn.imatu.framework.security;

import cn.imatu.framework.security.ratelimit.RateLimitProperties;
import cn.imatu.framework.security.xss.config.SecurityXssProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.annotation.PostConstruct;

/**
 * reactive
 *
 * @author shenguangyang
 */
@AutoConfiguration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
@EnableConfigurationProperties({RateLimitProperties.class, SecurityXssProperties.class})
public class LySecurityReactiveAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LySecurityAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
