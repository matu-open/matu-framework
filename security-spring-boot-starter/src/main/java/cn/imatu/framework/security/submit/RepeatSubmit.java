package cn.imatu.framework.security.submit;

import java.lang.annotation.*;

/**
 * 可以防止重复提交的注解, 底层使用redis
 * @author shenguangyang
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmit {
    /**
     * 防重提交方式
     */
    Type type() default Type.PARAM;

    /**
     * 间隔时间, 默认是5s, 5s之内不能重复提交
     */
    long time() default 5;

    /**
     * 防重提交, 支持两种方式, 一个是参数, 一个是令牌
     */
    enum Type {
        /**
         * 参数类型: 针对未登录的用户
         */
        PARAM,
        /**
         * 缓存中的key = token, 需要使用方调用指定的方法先获取token, 再拿着token请求
         */
        TOKEN
    }
}
