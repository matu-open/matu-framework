package cn.imatu.framework.security.xss.core;

import cn.imatu.framework.core.utils.spring.*;
import cn.imatu.framework.security.xss.config.*;
import cn.imatu.framework.security.xss.utils.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Objects;

/**
 * jackson xss 处理
 *
 * @author shenguangyang
 */
@Slf4j
public class XssCleanDeserializer extends XssCleanDeserializerBase {

    @Override
    public String clean(String name, String text) throws IOException {
        // 读取 xss 配置
        SecurityXssProperties properties = SpringUtils.getBean(SecurityXssProperties.class);
        // 读取 XssCleaner bean
        XssCleaner xssCleaner = SpringUtils.getBean(XssCleaner.class);
        if (Objects.isNull(xssCleaner) || Objects.isNull(properties)) {
            return text;
        }
        String value = xssCleaner.clean(XssUtils.trim(text, properties.isTrimText()));
        log.debug("Json property value:{} cleaned up by mica-xss, current value is:{}.", text, value);
        return value;
    }

}
