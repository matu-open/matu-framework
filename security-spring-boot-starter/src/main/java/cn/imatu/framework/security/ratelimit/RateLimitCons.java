package cn.imatu.framework.security.ratelimit;

/**
 * @author shenguangyang
 */
public interface RateLimitCons {
    /**
     * 剩余时间参数
     */
    String REMAINING_TIME_ARG = "${remainingTime}";
}
