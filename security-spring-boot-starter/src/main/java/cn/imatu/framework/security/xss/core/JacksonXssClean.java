package cn.imatu.framework.security.xss.core;

import cn.imatu.framework.security.xss.config.SecurityXssProperties;
import cn.imatu.framework.security.xss.utils.XssUtils;
import cn.hutool.core.util.ArrayUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Objects;

/**
 * jackson xss 处理
 *
 * @author shenguangyang
 */
@Slf4j
@RequiredArgsConstructor
public class JacksonXssClean extends XssCleanDeserializerBase {

    private final SecurityXssProperties properties;

    private final XssCleaner xssCleaner;

    @Override
    public String clean(String name, String text) throws IOException {
        if (XssHolder.isEnabled() && Objects.isNull(XssHolder.getXssCleanIgnore())) {
            String value = xssCleaner.clean(XssUtils.trim(text, properties.isTrimText()));
            log.debug("Json property value:{} cleaned up by mica-xss, current value is:{}.", text, value);
            return value;
        } else if (XssHolder.isEnabled() && Objects.nonNull(XssHolder.getXssCleanIgnore())) {
            XssCleanIgnore xssCleanIgnore = XssHolder.getXssCleanIgnore();
            if (ArrayUtil.contains(xssCleanIgnore.value(), name)) {
                return XssUtils.trim(text, properties.isTrimText());
            }

            String value = xssCleaner.clean(XssUtils.trim(text, properties.isTrimText()));
            log.debug("Json property value:{} cleaned up by mica-xss, current value is:{}.", text, value);
            return value;
        } else {
            return XssUtils.trim(text, properties.isTrimText());
        }
    }

}
