package cn.imatu.framework.security.submit;

import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * XSS跨站脚本配置
 *
 * @author shenguangyang
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "security.repeat-submit")
public class RepeatSubmitProperties {
    public static final String PREFIX = LyCoreConstants.PROPERTIES_PRE + "security.repeat-submit";
    /**
     * 开关
     */
    private boolean enabled = true;
}
