package cn.imatu.framework.security.submit;

import cn.imatu.framework.core.utils.servlet.ServletUtils;
import cn.imatu.framework.exception.BaseError;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.PostConstruct;
import java.util.function.Supplier;

/**
 * 重复提交的用户, 需要使用方自己进行配置
 * @author shenguangyang
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepeatSubmitDefinition {
    /**
     * 登录token-每次登录要唯一
     */
    private Supplier<String> tokenSupplier =
        () -> ServletUtils.getRequest().map(e -> e.getAttribute("loginId")).map(String::valueOf).orElse(null);

    /**
     * 当重复提交时候抛出的异常
     */
    private BaseError baseError;

    /**
     * 缓存key的前缀
     */
    @Builder.Default
    private String keyPrefix = "repeatSubmit";

    @PostConstruct
    public void init() {

    }

}
