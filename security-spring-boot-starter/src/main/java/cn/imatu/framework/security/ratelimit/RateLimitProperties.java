package cn.imatu.framework.security.ratelimit;

import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

/**
 * @author shenguangyang
 */
@Data
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "security.rate-limit")
public class RateLimitProperties {
    /**
     * 黑名单配置
     */
    private Blacklist blacklist = new Blacklist();

    @Data
    public static class Blacklist {
        /**
         * 是否使能黑名单, 如果使能了, 超出限制后会被加入到黑名单中
         */
        private boolean enabled;
        /**
         * 限制时间, 默认是2分钟
         */
        private Duration limitTime = Duration.ofMinutes(2);
    }
}
