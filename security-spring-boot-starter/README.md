# 公共安全包

主要对接口的保护, 比如

- 限流
- xxs攻击
- 对于同一个ip在一段时间内频繁访问进行限制
- api签名组件
- ...

## 签名

作用
- 防重放
- 防篡改

当前代码是服务端代码, 签名客户端代码gitee地址: https://gitee.com/sgy_project/project-common-sign-client

