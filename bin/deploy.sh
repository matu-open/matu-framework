#! /bin/bash

# 并发发布有问题, 可能会导致发布卡死
#mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -T 1C -DskipTests
mvn clean install deploy  -P release -DskipTests
# 部署到内网
#mvn clean install deploy  -P intranet-nexus -DskipTests -T 1C
mvn clean install deploy  -P intranet-nexus -DskipTests