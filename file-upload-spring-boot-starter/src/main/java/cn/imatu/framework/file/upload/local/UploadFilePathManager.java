package cn.imatu.framework.file.upload.local;

import cn.imatu.framework.file.upload.properties.FileUploadProperties;
import cn.imatu.framework.tool.core.exception.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用于管理上传的文件存放在本地的路径
 *
 * @author shenguangyang
 */
@Component
public class UploadFilePathManager {
    private final FileUploadProperties fileUploadProperties;

    @Autowired
    public UploadFilePathManager(FileUploadProperties fileUploadProperties) {
        this.fileUploadProperties = fileUploadProperties;
    }

    /**
     * 获取文件的上传路径
     *
     * @return 路径
     */
    public String getMergePath() {
        return fileUploadProperties.getRootDir() + fileUploadProperties.getMergeDir() + "/";
    }

    /**
     * 获取文件所在的目录
     *
     * @param fileCode 文件code值
     * @return 完成路径
     */
    public String getMergeFileFolderPath(String fileCode) {
        Assert.notNull(fileCode, "文件code不能为空");
        return getMergePath() + fileCode.charAt(0) + "/" + fileCode.charAt(1) + "/";
    }

    /**
     * 获取文件的完成路径
     *
     * @param fileCode 文件code
     * @param fileExt  文件扩展名
     * @return 完整路径
     */
    public String getMergeFilePath(String fileCode, String fileExt) {
        return getMergeFileFolderPath(fileCode) + fileCode + "." + fileExt;
    }

    /**
     * 获取分片文件所在目录
     *
     * @param fileCode 文件的code
     * @return 分片文件路径
     */
    public String getChunkFileFolderPath(String fileCode) {
        return fileUploadProperties.getRootDir() + fileUploadProperties.getChunkDir() + "/" + fileCode + "/";
    }
}
