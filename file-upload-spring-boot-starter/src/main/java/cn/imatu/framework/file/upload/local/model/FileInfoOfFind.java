package cn.imatu.framework.file.upload.local.model;

import cn.imatu.framework.file.upload.local.enums.FileUploadStateEnum;
import cn.imatu.framework.tool.core.exception.Assert;
import lombok.Getter;
import lombok.Setter;

/**
 * 存放从存储介子中查找到的文件信息 (比如数据库)
 *
 * @author shenguangyang
 */
@Getter
@Setter
public class FileInfoOfFind {
    private String id;
    private String filePath;
    private FileUploadStateEnum state;

    public static FileInfoOfFindBuilder builder() {
        return new FileInfoOfFindBuilder();
    }

    public static final class FileInfoOfFindBuilder {
        private String id;
        private String filePath;
        private FileUploadStateEnum state;

        private FileInfoOfFindBuilder() {
        }

        public FileInfoOfFindBuilder id(String id) {
            this.id = id;
            return this;
        }

        public FileInfoOfFindBuilder filePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public FileInfoOfFindBuilder state(FileUploadStateEnum state) {
            this.state = state;
            return this;
        }

        public FileInfoOfFind build() {
            FileInfoOfFind fileInfoOfFind = new FileInfoOfFind();
            fileInfoOfFind.setId(id);
            fileInfoOfFind.setFilePath(filePath);
            fileInfoOfFind.setState(state);
            Assert.notNull(state, "请设置文件上传状态");
            Assert.notEmpty(filePath, "请设置文件路径");
            return fileInfoOfFind;
        }
    }
}
