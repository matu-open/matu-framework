package cn.imatu.framework.file.upload.local.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.unit.DataSize;

import java.util.Map;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@Builder
public class BreakpointRegister {
    /**
     * 文件code, 可以是文件的md5, 只要保证不同的文件 fileCode不同即可
     */
    private String fileCode;
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件大小
     */
    private DataSize fileSize;
    /**
     * 文件类型 eg: application/pdf
     */
    private String contentType;
    /**
     * 文件后缀
     */
    private String fileExt;
    /**
     * 创建用户Id
     */
    private String createUserId;
    /**
     * 创建者用户名
     */
    private String createUserName;

    /**
     * 对象类型
     */
    private String objectType;

    private Map<String, Object> ext;
}
