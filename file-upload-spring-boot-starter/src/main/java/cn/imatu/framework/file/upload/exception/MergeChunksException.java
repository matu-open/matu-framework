package cn.imatu.framework.file.upload.exception;

import cn.imatu.framework.exception.BaseException;

/**
 * @author shenguangyang
 */
public class MergeChunksException extends BaseException {
    public MergeChunksException() {
    }

    public MergeChunksException(Integer code, String message) {
        super(code, message);
    }

    public MergeChunksException(String message) {
        super(message);
    }
}
