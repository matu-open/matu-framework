package cn.imatu.framework.file.upload.local.callback;

import cn.imatu.framework.file.upload.local.model.*;
import cn.imatu.framework.file.upload.local.model.*;

/**
 * 断点续传注册时回调接口
 *
 * @author shenguangyang
 */
public interface LocalBreakpointResumeCallback {
    /**
     * 从数据库中查找文件信息,
     */
    FileInfoOfFind find(BreakpointRegister breakpointRegister);

    /**
     * 数据库中是否存在对应的记录
     */
    boolean exist(MergeChunks mergeChunks);

    /**
     * 上传成功之后保存文件信息
     */
    void save(FileRegisterInfo fileRegisterInfo);

    /**
     * 合并文件之前
     */
    boolean mergeBefore(MergeChunks mergeChunks);

    /**
     * 合并文件之后操作
     */
    MergeChunksDTO mergeAfter(MergeChunks mergeChunks);
}
