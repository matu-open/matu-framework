package cn.imatu.framework.file.upload.local.enums;

/**
 * Description: 文件状态枚举
 * 1: 未上传完成 2: 已上传完成 -1: 转码失败
 *
 * @author shenguangyang
 */
public enum FileUploadStateEnum {
    /**
     * 未上传完成
     */
    NOT_UPLOAD_COMPLETED(1),
    /**
     * 已上传完成
     */
    UPLOADED(2),
    /**
     * 超出限制
     */
    SIZE_EXCEEDED(-1);

    private final int state;

    FileUploadStateEnum(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }
}
