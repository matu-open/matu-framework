package cn.imatu.framework.ip2region;

import cn.imatu.framework.ip2region.config.Geoip2Config;
import cn.imatu.framework.ip2region.config.properties.LyIpProperties;
import cn.imatu.framework.ip2region.config.LyGeoip2AutoConfiguration;
import cn.imatu.framework.ip2region.config.LyIp2RegionAutoConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Slf4j
@AutoConfiguration
@Import({
        Geoip2Config.class, LyGeoip2AutoConfiguration.class, LyIp2RegionAutoConfiguration.class
})
@EnableConfigurationProperties({
        LyIpProperties.class
})
public class LyIp2regionAutoConfiguration {

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
