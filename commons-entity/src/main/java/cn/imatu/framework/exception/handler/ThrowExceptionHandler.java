package cn.imatu.framework.exception.handler;

import cn.imatu.framework.exception.BaseError;
import cn.imatu.framework.exception.BaseException;
import cn.imatu.framework.exception.BizException;

import java.util.function.Supplier;

/**
 * @author shenguangyang
 */
public class ThrowExceptionHandler {
    private final boolean checkSuccess;

    public ThrowExceptionHandler(boolean checkSuccess) {
        this.checkSuccess = checkSuccess;
    }

    /**
     * 抛出异常信息
     *
     * @param message 异常信息
     **/
    public void orThrow(String message) {
        if (checkSuccess) {
            return;
        }
        throw new BizException(message);
    }

    public void orThrow(String message, String code) {
        if (checkSuccess) {
            return;
        }
        throw new BizException(message, code);
    }

    public void orThrow(BaseError baseError) {
        if (checkSuccess) {
            return;
        }
        throw new BizException(baseError);
    }

    public void orThrow(Supplier<BaseException> supplier) {
        if (checkSuccess) {
            return;
        }
        throw supplier.get();
    }
}
