package cn.imatu.framework.exception;

/**
 * 参数无效异常
 *
 * @author shenguangyang
 */
public class ArgumentNotValidException extends BaseException {
    public ArgumentNotValidException() {
    }

    public ArgumentNotValidException(Integer code, String message) {
        super(code, message);
    }

    public ArgumentNotValidException(String message) {
        super(message);
    }

    public ArgumentNotValidException(BaseError baseError) {
        super(baseError);
    }

    public ArgumentNotValidException(String msgTemplate, Object... msgValues) {
        super(msgTemplate, msgValues);
    }

    public ArgumentNotValidException(Integer code, String msgTemplate, Object... msgValues) {
        super(code, msgTemplate, msgValues);
    }
}
