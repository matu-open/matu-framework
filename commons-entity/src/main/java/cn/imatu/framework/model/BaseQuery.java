package cn.imatu.framework.model;

import cn.imatu.framework.exception.BizException;
import com.alibaba.fastjson2.JSONObject;

import java.io.Serializable;
import java.util.Objects;


/**
 * 基础查询
 * @author shenguangyang
 */
public class BaseQuery implements Serializable {

    /**
     * 校验是否全部为空
     */
    public void verifyIsAllNull() {
        if (JSONObject.from(this).values().stream().allMatch(Objects::isNull)) {
            throw new BizException("查询条件不能全部为空");
        }
    }

    public static class Test extends BaseQuery {

    }
    public static void main(String[] args) {

    }
}
