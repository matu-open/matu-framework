package cn.imatu.framework.validation;

import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.NotEmpty;

/**
 * @author shenguangyang
 */
public class AnnoTest {
    @Getter
    @Setter
    public static class MyReq {
        @NotEmpty(message = "不能为空")
        @StrToDate(message = "非法日期, 格式为yyyy-MM-dd HH:mm", format = "yyyy-MM-dd HH:mm")
        private String date;

        @IsIdCard(message = "非法身份证号")
        private String idCard;

        @IsPhone(message = "非法手机号")
        private String phone;
    }

    @Test
    public void testStrToDate() {
        MyReq req = new MyReq();
        req.setDate("2022-12-12 12:00");
        ValidationUtils.validateFast(req);
    }

    @Test
    public void test() {
        MyReq testReq = new MyReq();
        testReq.setDate("2022-12-12 12:00");
        testReq.setIdCard("66885620671120257X");
        testReq.setPhone("13938592313");
        ValidationUtils.validateFast(testReq);
    }
}
