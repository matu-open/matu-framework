package cn.imatu.framework.data.mate.fieldbind.inter;

import cn.imatu.framework.data.mate.fieldbind.model.DictType;

import java.util.List;

/**
 * @author shenguangyang
 */
public interface IDataBind<T> {
    /**
     * 通过对象中所有字典type获取每个type对应的code数据集
     *
     * @param dictTypes 字典类型集合, 所有对象同一个字典类型会被转成一个 {@link DictType}对象的,
     *                  同时内部也对注解所在的字段值进行了去重
     * @return types对应的类型集合, 一般情况下会返回一个map集合, key = type, value = valueOfType(或者集合)
     */
    default T getCodesOfTypes(List<DictType> dictTypes) {
        return null;
    }

    /**
     * 获取code对应的值
     * @param codesOfTypes 对象中所有字典type对应的字典code数据集, 来自 {@link #getCodesOfTypes(List)} 返回结果
     * @param type 字典类型
     * @param code 编码
     * @return 编码对应的值
     */
    String getCodeValue(T codesOfTypes, String type, String code);
}
