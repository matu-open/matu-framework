package cn.imatu.framework.data.mate.fieldconvert.inter;

import cn.imatu.framework.data.mate.fieldconvert.model.AnnotInfo;

import java.lang.annotation.Annotation;

/**
 * @author shenguangyang
 */
@FunctionalInterface
public interface ICustomFieldConvertAnnotGetter<T extends Annotation> {
    AnnotInfo annotInfo(T annot);
}
