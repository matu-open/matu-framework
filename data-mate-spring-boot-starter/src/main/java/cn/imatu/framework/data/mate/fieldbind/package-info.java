/**
 * 字段绑定(字典翻译), 支持嵌套对象、数组中对象、集合中对象的翻译
 *
 * @author shenguangyang
 */
package cn.imatu.framework.data.mate.fieldbind;