package cn.imatu.framework.data.mate.fieldsensitive.model;

import cn.imatu.framework.data.mate.annotations.FieldSensitive;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class SensitiveClassCache {
    private Class<?> clazz;
    /**
     * 添加了 {@link FieldSensitive} 注解的字段
     */
    private Set<FieldOfBaseType> fields;

    public SensitiveClassCache() {
        this.fields = new CopyOnWriteArraySet<>();
    }

    /**
     * 基本类型的字段数据, 这里的基本类型包含 String, 基本类型包装类, 基本类型非包装类
     */
    @Getter
    @Setter
    public static class FieldOfBaseType {
        private FieldSensitive annoData;
        private Field field;
    }
}
