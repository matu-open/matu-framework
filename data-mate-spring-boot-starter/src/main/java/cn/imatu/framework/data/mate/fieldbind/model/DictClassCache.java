package cn.imatu.framework.data.mate.fieldbind.model;

import cn.imatu.framework.data.mate.annotations.FieldBind;
import cn.imatu.framework.data.mate.fieldbind.inter.IFieldBindAnnotationDataGetter;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 存放需要实体类中标注 {@link FieldBind} 注解的信息
 *
 * @author shenguangyang
 */
@Getter
@Setter
public class DictClassCache {
    private Class<?> clazz;
    /**
     * 添加了 {@link FieldBind} 注解或者是{@link IFieldBindAnnotationDataGetter} 中指定的泛型注解
     * 的字段
     */
    private List<FieldDefine> fieldList;

    private Set<String> types;

    public DictClassCache() {
        this.fieldList = new ArrayList<>();
    }

    public void fieldAnnotationTypes() {
        this.types = this.fieldList.stream().map(x -> x.getAnnotationMetadata().getType()).collect(Collectors.toSet());
    }
}
