package cn.imatu.framework.data.mate.fieldbind;

import cn.imatu.framework.data.mate.fieldbind.inter.IDataBind;
import cn.imatu.framework.data.mate.fieldbind.model.DictType;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shenguangyang
 */
@Slf4j
public class DefaultDataBind implements IDataBind<Map<String, Map<String, String>>> {
    private static final Map<String, String> EMPTY_MAP = new HashMap<>();
    @Override
    public Map<String, Map<String, String>> getCodesOfTypes(List<DictType> dictTypes) {
        Map<String, Map<String, String>> defaultDictMap = new HashMap<>();
        for (DictType dictType : dictTypes) {
            Map<String, String> valueMap = new HashMap<>();
            valueMap.put("1", "1-v");
            valueMap.put("2", "2-v");
            valueMap.put("3", "3-v");
            defaultDictMap.put(dictType.getType(), valueMap);
        }
        return defaultDictMap;
    }

    @Override
    public String getCodeValue(Map<String, Map<String, String>> codesOfTypes, String type, String code) {
        return codesOfTypes.getOrDefault(type, Collections.emptyMap()).get(code);
    }
}
