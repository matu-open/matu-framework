package cn.imatu.framework.data.mate.fieldbind;

import cn.imatu.framework.data.mate.fieldbind.inter.IFieldBindAnnotationDataGetter;
import cn.imatu.framework.data.mate.annotations.FieldBind;
import cn.imatu.framework.data.mate.fieldbind.model.AnnotationMetadata;
import org.springframework.stereotype.Component;

/**
 * @author shenguangyang
 */
@Component
public class DefaultFieldBindAnnotationGetter implements IFieldBindAnnotationDataGetter<FieldBind> {
    @Override
    public AnnotationMetadata initFieldBindAnnotation(FieldBind fieldBind) {
        return AnnotationMetadata.builder().type(fieldBind.type())
            .target(fieldBind.target())
            .targetEqCode(fieldBind.targetEqCode())
            .mapperTarget(fieldBind.mapperTarget()).codeDefault(fieldBind.codeDefault())
            .delimiter(fieldBind.delimiter()).targetDelimiter(fieldBind.targetDelimiter())
            .build();
    }
}
