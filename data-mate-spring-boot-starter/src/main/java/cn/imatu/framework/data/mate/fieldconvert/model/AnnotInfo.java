package cn.imatu.framework.data.mate.fieldconvert.model;

import cn.imatu.framework.data.mate.annotations.FieldBind;
import com.alibaba.fastjson2.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.lang.annotation.Annotation;

/**
 * @see FieldBind
 * @author shenguangyang
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnnotInfo {
    /**
     * 分隔每个元素的分隔符号
     * @apiNote 只有在字段类型是 {@code String} 时才会生效
     */
    private String delimiter;

    private String targetField;

    // ==================================== 以下字段不需要用户定义
    private String sourceField;

    /**
     * 自定义注解
     */
    private Annotation customAnnot;

    /**
     * 扩充数据
     */
    private JSONObject extData = new JSONObject();
}
