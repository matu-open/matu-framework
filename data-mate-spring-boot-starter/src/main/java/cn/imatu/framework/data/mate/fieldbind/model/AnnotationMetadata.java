package cn.imatu.framework.data.mate.fieldbind.model;

import cn.imatu.framework.data.mate.annotations.FieldBind;
import lombok.*;

import java.util.Objects;

/**
 * @see FieldBind
 * @author shenguangyang
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnnotationMetadata {
    private String type;

    /**
     * @see FieldBind#targetEqCode()
     */
    private Boolean targetEqCode;

    /**
     * 数据绑定到哪个字段上
     */
    private String target;

    /**
     * code的默认值
     */
    private String codeDefault;

    /**
     * 映射的字段
     * @see FieldBind#codeDefault()
     */
    private String mapperTarget;

    /**
     * 分隔每个元素的分隔符号
     * <p>
     * 当前注解所在字符串字段code时候, 每个code采用什么分割这里要指明 <br/>
     * eg:
     * <blockquote><pre>
     * {@literal @FieldBind(type = "test", target = "testText", delimiter = "|")}
     *  private String test;
     *  private String testText;
     * </pre></blockquote>
     * 当 test = 1|2|3(假如code = 1 对应 data1, 依次类推), 则testText将被赋值为
     * testText = data1|data2|data3 <br/>
     * 目标字段分隔符, 请看 {@code targetDelimiter}
     * </p>
     *
     * @apiNote 只有在字段类型是 {@code String} 时才会生效
     */
    private String delimiter;

    /**
     * 目标字段分隔符
     */
    private String targetDelimiter;

    public Boolean getTargetEqCode() {
        return !Objects.isNull(targetEqCode) && targetEqCode;
    }
}
