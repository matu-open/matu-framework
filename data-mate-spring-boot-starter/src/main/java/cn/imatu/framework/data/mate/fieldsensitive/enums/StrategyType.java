package cn.imatu.framework.data.mate.fieldsensitive.enums;

/**
 * 枚举需要脱敏的字段类型
 *
 * @author shenguangyang
 */
public interface StrategyType {

    String CUSTOM = "custom";

    /**
     * 置空
     */
    String EMPTY = "empty";

    /**
     * 中文名
     */
    String CHINESE_NAME = "chineseName";

    /**
     * 身份证号
     */
    String ID_CARD = "idCard";

    /**
     * 座机号
     */
    String FIXED_PHONE = "fixedPhone";

    /**
     * 手机号
     */
    String MOBILE_PHONE = "mobilePhone";


    /**
     * 电子邮件
     */
    String EMAIL = "email";

    /**
     * 银行卡
     */
    String BANK_CARD = "bankCard";

    /**
     * 公司开户银行联号
     */
    String BANK_NUMBER = "bankNumber";

    /**
     * 保留4位明文数据
     */
    String RESERVE_4 = "reserve_4";
    String RESERVE_6 = "reserve_6";
    String RESERVE_8 = "reserve_8";
}
