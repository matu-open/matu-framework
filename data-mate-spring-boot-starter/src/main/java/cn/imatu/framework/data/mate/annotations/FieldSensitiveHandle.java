package cn.imatu.framework.data.mate.annotations;

import java.lang.annotation.*;

/**
 * 自动处理敏感信息, 在某个方法上使用, 则返回的对象数据会被进行数据脱敏处理
 *
 * @author shenguangyang
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.ANNOTATION_TYPE})
public @interface FieldSensitiveHandle {

}

