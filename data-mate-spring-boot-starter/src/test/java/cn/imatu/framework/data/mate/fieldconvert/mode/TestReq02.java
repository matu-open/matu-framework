package cn.imatu.framework.data.mate.fieldconvert.mode;

import cn.imatu.framework.data.mate.annotations.FieldConvert;
import cn.imatu.framework.data.mate.fieldconvert.FileUrlConvert;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class TestReq02 extends BaseTestReq02 {
    private List<Test1> test16 = new ArrayList<Test1>() {{
        add(new Test1());
    }};

    private List<Test1> test16_1 = new ArrayList<Test1>() {{
        add(new Test1());
    }};

    @FieldConvert(targetField = "key2")
    private String url2 = "https://test2";
    private String key2;

    @FieldConvert(targetField = "key3")
    private String url3 = "https://test3,https://test4";
    private List<String> key3;

    @FieldConvert(targetField = "key4")
    private Set<String> url4 = new HashSet<>();
    private List<String> key4;

    @FieldConvert(targetField = "url5")
    private Set<String> url5 = new HashSet<>();

    @FieldConvert(targetField = "url6")
    private String url6 = "https://test6";

    public TestReq02() {
        this.url4.add("https://test4.1");
        this.url4.add("https://test4.2");

        this.url5.add("https://test5.1");
        this.url5.add("https://test5.2");
    }

    @Data
    public static class Test1 {
        @FileUrlConvert(targetField = "key20")
        private String url20 = "https://test20";
        private String key20;

        public void clearUrl() {
            url20 = null;
        }
    }


    public void clearUrl() {
        this.url4 = null;
        this.url5 = null;
        this.url6 = null;
        this.url2 = null;
        this.url3 = null;

        this.test16.forEach(Test1::clearUrl);
        this.test16_1.forEach(Test1::clearUrl);
    }
}
