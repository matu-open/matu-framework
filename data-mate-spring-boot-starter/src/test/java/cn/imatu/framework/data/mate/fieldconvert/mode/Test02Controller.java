package cn.imatu.framework.data.mate.fieldconvert.mode;

import cn.imatu.framework.data.mate.annotations.FieldConvertHandle;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shenguangyang
 */
@RestController
@RequestMapping("/test")
public class Test02Controller {

    @FieldConvertHandle
    public TestReq02 testReq02(TestReq02 testReq02) {
        testReq02.clearUrl();
        return testReq02;
    }
}
