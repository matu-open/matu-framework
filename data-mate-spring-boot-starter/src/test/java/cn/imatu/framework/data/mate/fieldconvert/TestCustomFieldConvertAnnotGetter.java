package cn.imatu.framework.data.mate.fieldconvert;

import cn.imatu.framework.data.mate.fieldconvert.inter.ICustomFieldConvertAnnotGetter;
import cn.imatu.framework.data.mate.fieldconvert.model.AnnotInfo;
import org.springframework.stereotype.Component;

/**
 * @author shenguangyang
 */
@Component
public class TestCustomFieldConvertAnnotGetter implements ICustomFieldConvertAnnotGetter<FileUrlConvert> {

    @Override
    public AnnotInfo annotInfo(FileUrlConvert annot) {
        return AnnotInfo.builder().delimiter(annot.delimiter()).targetField(annot.targetField()).build();
    }
}
