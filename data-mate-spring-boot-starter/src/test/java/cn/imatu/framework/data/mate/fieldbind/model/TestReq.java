package cn.imatu.framework.data.mate.fieldbind.model;

import cn.imatu.framework.data.mate.annotations.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class TestReq extends BaseTestReq {
    private String otherValue = "哈哈哈啦啦啦";

//    @FieldBind(type = "test", target = "test1", mapperTarget = "otherValue")
    private String name = "2";

//    private String test1;
//
//    @FieldBind(type = "test", target = "test2", targetEqCode = true)
//    private String name2 = "200";
//    private String test2;
//
//    @FieldBind(type = "test", target = "test3", targetEqCode = true)
//    private Integer[] name3 = new Integer[] {2};
//    private String test3;

//    @FieldBind(type = "test", target = "test4", targetEqCode = true)
//    private Integer[] name4 = new Integer[] {2};
//    private List<String> test4;

//    @FieldBind(type = "test", target = "test5", targetEqCode = true)
//    private Integer[] name5 = new Integer[] {2};
//    private Set<String> test5;


//    @FieldBind(type = "test", target = "test6", targetEqCode = true)
//    private List<Integer> name6 = Collections.singletonList(2);
//    private Set<String> test6;


//    @FieldBind(type = "test", target = "test7", targetEqCode = true)
//    private List<String> name7 = Collections.singletonList("2");
//    private Set<String> test7;

//    @FieldBind(type = "test", target = "test8", targetEqCode = true)
//    private List<String> name8 = Collections.singletonList("2");
//    private String[] test8;

//    @FieldBind(type = "test", target = "test9", targetEqCode = true, codeDefault = "99", mapperTarget = "otherValue")
//    private List<String> name9 = Collections.singletonList("99");
//    private String[] test9;

//    @FieldBind(type = "test", target = "test10", targetEqCode = true, codeDefault = "99", mapperTarget = "otherValue")
//    private List<String> name10 = Collections.singletonList("100");
//    private List<String> test10;


//    @FieldBind(type = "test", target = "test11", targetEqCode = true, targetDelimiter = "、")
//    private List<String> name11 = Arrays.asList("1", "2");
//    private String test11;
//
//    @FieldBind(type = "test", target = "test12", targetEqCode = true, delimiter = ",", targetDelimiter = "、")
//    private String name12 = "1,2";
//    private String test12;
//
//    @FieldBind(type = "test", target = "test13", targetEqCode = true, delimiter = ",", targetDelimiter = "、")
//    private String name13 = "1,2";
//    private List<String> test13;


//    @FieldBind(type = "test", target = "test14", targetEqCode = true)
//    private Integer name14 = 1;
//    private List<String> test14;

//    @FieldBind(type = "test", target = "test15", targetEqCode = true)
//    private Integer name15 = null;
//    private List<String> test15;



    private List<Test1> test16 = new ArrayList<Test1>() {{
        add(new Test1());
    }};

    private List<Test1> test16_1 = new ArrayList<Test1>() {{
        add(new Test1());
    }};

    @FieldBind(type = "test", target = "test17", targetEqCode = true)
    private Integer name17 = 1;
    private List<String> test17;

    @FieldBind(type = "test", target = "test18", targetEqCode = true)
    private Integer name18 = 1;
    private List<String> test18;

    @Data
    public static class Test1 {
        @FieldBind(type = "test", target = "name1", targetEqCode = true)
        private String code1 = "1";
        private String name1;
    }
}
